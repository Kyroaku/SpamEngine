#pragma once

#include <stdio.h>
#include <SDL/SDL.h>
#include <string>
#include "dllmain.h"
#include "Vector.h"

#pragma comment(lib, "SDL2")

using std::string;

namespace Spam
{
	class SPAMAPI Window
	{
		SDL_Window *window;
		SDL_GLContext context;

		bool init();
		void create(const char *title, int width, int height, short bitsPerPixel, bool fullscreen);

	public:
		Window(const char *title, int width, int height, short bitsPerPixel, bool fullscreen);
		~Window();
		SDL_Window *getHandle();
		SDL_GLContext getGLContext();
		const char *getTitle();
		void setTitle(const char *title);
		Spam::Vector2 getPosition();
		void setPosition(int x, int y);
		Spam::Vector2 getSize();
		void setSize(int w, int h);
		void setMousePosition(int x, int y);
	};
};