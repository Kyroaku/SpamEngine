#pragma once

/* for(unsigned int i = 0; i < n; i++) */
#define loop(i, n) for(unsigned int i = 0; i < n; i++)
/* for(unsigned int i = 0; i < n; i+=s) */
#define loops(i, n, s) for(unsigned int i = 0; i < n; i+=s)