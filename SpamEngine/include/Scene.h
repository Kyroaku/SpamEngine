#pragma once

#include "Node.h"
#include "Mesh.h"
#include "Material.h"
#include "Animation.h"
#include "assimp\Importer.hpp"
#include "assimp\scene.h"
#include "assimp\postprocess.h"

#pragma comment(lib, "assimp")

#define SPAM_POSITION_ARRAY_INDEX		0
#define SPAM_TEXTURECOORD_ARRAY_INDEX	1
#define SPAM_COLOR_ARRAY_INDEX			2
#define SPAM_NORMAL_ARRAY_INDEX			3
#define SPAM_TANGENT_ARRAY_INDEX		4
#define SPAM_BITANGENT_ARRAY_INDEX		5
#define SPAM_BONE_ID_ARRAY_INDEX		6
#define SPAM_BONE_WEIGHT_ARRAY_INDEX	7

namespace Spam
{
	class SPAMAPI Scene
	{
		void copyNode(Spam::Node *node, Spam::Node *parent, aiNode *ainode);

	public:
		Spam::Mesh **meshes;
		unsigned int numMeshes;
		Spam::Node *rootNode;
		Spam::Material **materials;
		unsigned int numMaterials;
		Spam::Animation **animations;
		unsigned int numAnimations;

		unsigned int currentAnimation;
		double animationTime;
		bool animating;

		Scene();
		Scene(const char *path);
		~Scene();
		void loadFromFile(const char *path);
		void update(double dt);
		void startAnimation();
		void stopAnimation();
		bool isAnimating();
		void setAnimationTime(double time);
		void setAnimation(unsigned int animation_id);
	};
};