#pragma once

namespace Spam
{
	//Virtual classes
	class Renderer
	{
	public:
		virtual void onRender() {}
		virtual void onUpdate(float deltaTime) {}
		virtual void onResize(int w, int h) {}
	};

	class Keyboard
	{
	public:
		bool keys[256];

		Keyboard()
		{
			for (int i = 0; i < 256; i++)
				keys[i] = false;
		}
		virtual void onKeyDown(short key) {}
		virtual void onKeyUp(short key) {}
	};

	class Mouse
	{
	public:
		bool buttons[16];
		int x, y;

		Mouse()
		{
			for (int i = 0; i < 16; i++)
				buttons[i] = false;
			x = y = 0;
		}
		virtual void onMove(int x, int y, int dx, int dy) {}
		virtual void onButtonDown(short button) {}
		virtual void onButtonUp(short button) {}
		virtual void onWheel(int x, int y) {}
	};

	const unsigned short KEY_A = SDLK_a;
	const unsigned short KEY_B = SDLK_b;
	const unsigned short KEY_C = SDLK_c;
	const unsigned short KEY_D = SDLK_d;
	const unsigned short KEY_E = SDLK_e;
	const unsigned short KEY_F = SDLK_f;
	const unsigned short KEY_G = SDLK_g;
	const unsigned short KEY_H = SDLK_h;
	const unsigned short KEY_I = SDLK_i;
	const unsigned short KEY_J = SDLK_j;
	const unsigned short KEY_K = SDLK_k;
	const unsigned short KEY_L = SDLK_l;
	const unsigned short KEY_M = SDLK_m;
	const unsigned short KEY_N = SDLK_n;
	const unsigned short KEY_O = SDLK_o;
	const unsigned short KEY_P = SDLK_p;
	const unsigned short KEY_Q = SDLK_q;
	const unsigned short KEY_R = SDLK_r;
	const unsigned short KEY_S = SDLK_s;
	const unsigned short KEY_T = SDLK_t;
	const unsigned short KEY_U = SDLK_u;
	const unsigned short KEY_W = SDLK_w;
	const unsigned short KEY_V = SDLK_v;
	const unsigned short KEY_X = SDLK_x;
	const unsigned short KEY_Y = SDLK_y;
	const unsigned short KEY_Z = SDLK_z;
	const unsigned short KEY_0 = SDLK_0;
	const unsigned short KEY_1 = SDLK_1;
	const unsigned short KEY_2 = SDLK_2;
	const unsigned short KEY_3 = SDLK_3;
	const unsigned short KEY_4 = SDLK_4;
	const unsigned short KEY_5 = SDLK_5;
	const unsigned short KEY_6 = SDLK_6;
	const unsigned short KEY_7 = SDLK_7;
	const unsigned short KEY_8 = SDLK_8;
	const unsigned short KEY_9 = SDLK_9;
	const unsigned short KEY_F1 = SDLK_F1;
	const unsigned short KEY_F2 = SDLK_F2;
	const unsigned short KEY_F3 = SDLK_F3;
	const unsigned short KEY_F4 = SDLK_F4;
	const unsigned short KEY_F5 = SDLK_F5;
	const unsigned short KEY_F6 = SDLK_F6;
	const unsigned short KEY_F7 = SDLK_F7;
	const unsigned short KEY_F8 = SDLK_F8;
	const unsigned short KEY_F9 = SDLK_F9;
	const unsigned short KEY_F10 = SDLK_F10;
	const unsigned short KEY_F11 = SDLK_F11;
	const unsigned short KEY_F12 = SDLK_F12;
	const unsigned short KEY_DOWN = SDLK_DOWN;
	const unsigned short KEY_UP = SDLK_UP;
	const unsigned short KEY_LEFT = SDLK_LEFT;
	const unsigned short KEY_RIGHT = SDLK_RIGHT;
	const unsigned short KEY_ESC = SDLK_ESCAPE;

	const unsigned short BUTTON_LEFT = SDL_BUTTON_LEFT;
	const unsigned short BUTTON_MIDDLE = SDL_BUTTON_MIDDLE;
	const unsigned short BUTTON_RIGHT = SDL_BUTTON_RIGHT;
};