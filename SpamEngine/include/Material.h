#pragma once

#include "Vector.h"
#include "Texture.h"

namespace Spam
{
	class SPAMAPI Material
	{
	public:
		Spam::Vector3 ambient, diffuse, specular, emissive;
		float shininess;
		Spam::Texture *texture;

		Material();
		~Material();
	};
};