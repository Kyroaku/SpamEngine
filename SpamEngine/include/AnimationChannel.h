#pragma once

#include "Node.h"
#include "ChannelFrameKey.h"

namespace Spam
{
	class SPAMAPI AnimationChannel
	{
	public:
		Spam::Node *node;
		unsigned int numKeys;
		Spam::ChannelFrameKey **keys;

		AnimationChannel();
		~AnimationChannel();
	};
};