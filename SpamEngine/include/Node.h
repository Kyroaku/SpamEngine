#pragma once

#include <string>

#include "Macros.h"
#include "Matrix.h"

namespace Spam
{
	class SPAMAPI Node
	{
	public:
		char *name;
		unsigned int *meshes;
		unsigned int numMeshes;
		Spam::Node **childrens;
		unsigned int numChildrens;
		Spam::Node *parent;
		Spam::Matrix4 transformation;

		Node();
		~Node();
		Node *findNode(const char *name);
		Spam::Matrix4 getAbsoluteMatrix();
	};
};