#pragma once

#include "Vector.h"
#include "Geometry.h"
#include "Texture.h"

namespace Spam
{
	class SPAMAPI Mesh
	{
	public:
		Mesh();
		Mesh(Spam::Geometry *geometry, Spam::Texture *texture);
		~Mesh();
		void setTranslation(float x, float y, float z);
		void setRotation(float x, float y, float z);
		void setScale(float x, float y, float z);
		void setTranslation(Spam::Vector4 v);
		void setRotation(Spam::Vector4 v);
		void setScale(Spam::Vector4 v);
		Spam::Vector4 getTranslation();
		Spam::Vector4 getRotation();
		Spam::Vector4 getScale();
		void setGeometry(Spam::Geometry *geometry);
		void setTexture(Spam::Texture *texture);
		Spam::Geometry *getGeometry();
		Spam::Texture *getTexture();
		bool hasGeometry();
		bool hasTexture();
		void initChildrens(int n);
		void setChildren(int i, Spam::Mesh *child);
		Spam::Mesh *getChildren(int i);
		int getChildrenCount();

	private:
		Spam::Vector4 translation;
		Spam::Vector4 rotation;
		Spam::Vector4 scale;
		Spam::Geometry *geometry;
		Spam::Texture *texture;
		Mesh **childs;
		int numChilds;
	};
}