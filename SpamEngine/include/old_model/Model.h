#pragma once

#include "dllmain.h"
#include "Mesh.h"
#include "Geometry.h"
#include "Texture.h"

namespace Spam
{
	class SPAMAPI Model
	{
	public:
		Model();
		~Model();
		void initMeshes(int n);
		void setMesh(int i, Spam::Mesh *mesh);
		Spam::Mesh *getMesh(int i);
		int getMeshCount();

	private:
		Spam::Mesh **meshes;
		int numMeshes;
	};
}