#pragma once

#include "dllmain.h"
#include "Geometry.h"

namespace Spam
{
	class SPAMAPI GeometryBuilder
	{
	public:
		static Spam::Geometry *rect(float w, float h, int sx, int sy)
		{
			Spam::Geometry *g = new Spam::Geometry();
			int numControlPoints = (sx + 1)*(sy + 1);
			int numIndices = sx*sy * 6;
			g->initVertexPositions(numControlPoints);
			g->initVertexColors(numControlPoints);
			g->initVertexTextureCoords(numControlPoints);
			g->initVertexNormals(numControlPoints);
			g->initVertexIndices(numIndices);
			for (int cp = 0; cp < numControlPoints; cp++)
			{
				g->getVertexPositionsData()[cp * 4 + 0] = w * (float)( (cp % (sx + 1)) / sx);
				g->getVertexPositionsData()[cp * 4 + 1] = h * (float)( (cp / (sx + 1)) / sy);
				g->getVertexPositionsData()[cp * 4 + 2] = 0.0f;
				g->getVertexPositionsData()[cp * 4 + 3] = 1.0f;

				g->getVertexColorsData()[cp * 4 + 0] =
				g->getVertexColorsData()[cp * 4 + 1] =
				g->getVertexColorsData()[cp * 4 + 2] =
				g->getVertexColorsData()[cp * 4 + 3] = 1.0f;
			}
			for (int i = 0; i < numIndices/6; i++)
			{
				g->getVertexIndicesData()[i * 6 + 0] = (i%sx) + (i / sy)*(sx + 1);
				g->getVertexIndicesData()[i * 6 + 1] = (i%sx) + (i / sy)*(sx + 1) + 1;
				g->getVertexIndicesData()[i * 6 + 2] = (i%sx) + (i / sy)*(sx + 1) + sx + 2;
				g->getVertexIndicesData()[i * 6 + 3] = (i%sx) + (i / sy)*(sx + 1) + sx + 2;
				g->getVertexIndicesData()[i * 6 + 4] = (i%sx) + (i / sy)*(sx + 1) + sx + 1;
				g->getVertexIndicesData()[i * 6 + 5] = (i%sx) + (i / sy)*(sx + 1);
			}
			return g;
		}
	};
}