#pragma once

#include <Windows.h>
#include <GL\glew.h>
#include <GL\GL.h>
#include "dllmain.h"
#include "Vector.h"

namespace Spam
{
	class SPAMAPI Geometry
	{
	public:
		Geometry();
		~Geometry();
		void initVertexPositions(int n);
		void initVertexColors(int n);
		void initVertexTextureCoords(int n);
		void initVertexNormals(int n);
		void initVertexIndices(int n);
		void setVertexPositions(Spam::Vector4 *v, int n);
		void setVertexColors(Spam::Vector4 *v, int n);
		void setVertexTextureCoords(Spam::Vector4 *v, int n);
		void setVertexNormals(Spam::Vector4 *v, int n);
		void setVertexPositions(float *v, int n);
		void setVertexColors(float *v, int n);
		void setVertexTextureCoords(float *v, int n);
		void setVertexNormals(float *v, int n);
		void setVertexIndices(int *v, int n);
		void setPolygonMode(int mode);
		float *getVertexPositionsData();
		float *getVertexColorsData();
		float *getVertexTextureCoordsData();
		float *getVertexNormalsData();
		int *getVertexIndicesData();
		int getPolygonMode();
		bool hasVertexPositions();
		bool hasVertexColors();
		bool hasVertexTextureCoords();
		bool hasVertexNormals();
		bool hasVertexIndices();
		int getNumVertices();
		int getNumIndices();

	private:
		float *vertexPositions;
		float *vertexColors;
		float *vertexTextureCoords;
		float *vertexNormals;
		float *vertexTangents;
		float *vertexBitangents;
		int *vertexIndices;
		int numVertices;
		int numIndices;
		int polygonMode;
	};
};