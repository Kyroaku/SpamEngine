#pragma once

#include "Matrix.h"

namespace Spam
{
	class SPAMAPI ChannelFrameKey
	{
	public:
		Spam::Matrix4 transformation;
		double time;

		ChannelFrameKey();
		~ChannelFrameKey();
	};
};