#pragma once

#include <math.h>
#include "dllmain.h"
#include "Vector.h"

namespace Spam
{
	class Vector4;
	class Vector3;
	class Vector2;

	class SPAMAPI Matrix2
	{
	public:
		float data[4];

		Matrix2();
		Matrix2(float v);
		Matrix2(float *v);
		Matrix2(float a1, float a2, float b1, float b2);
		Matrix2(Vector2 a, Vector2 b);
		~Matrix2();
		void copy(Spam::Matrix2 M);
		void copy(float *v);
		float *get();
		float get(int i);
		float get(int i, int j);
		void set(int i, float v);
		void set(int i, int j, float v);
		void set(float *v);
		void set(float a1, float a2, float b1, float b2);
		void set(Vector2 a, Vector2 b);
		float operator[](int i);
		void operator=(Matrix2 M);
		void operator==(Matrix2 M);
		void operator+=(Matrix2 M);
		void operator+=(float v);
		void operator-=(Matrix2 M);
		void operator-=(float v);
		void operator*=(Matrix2 M);
		void operator*=(float v);
		void operator/=(float v);
		Matrix2 operator+(Matrix2 M);
		Matrix2 operator+(float v);
		Matrix2 operator-(Matrix2 M);
		Matrix2 operator-(float v);
		Matrix2 operator*(Matrix2 M);
		Vector2 operator*(Vector2 v);
		Matrix2 operator*(float v);
		Matrix2 operator/(float v);
	};

	class SPAMAPI Matrix3
	{
	public:
		float data[9];

		Matrix3();
		Matrix3(float v);
		Matrix3(float *v);
		Matrix3(float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3);
		Matrix3(Vector3 a, Vector3 b, Vector3 c);
		~Matrix3();
		void copy(Spam::Matrix3 M);
		void copy(float *v);
		float *get();
		float get(int i);
		float get(int i, int j);
		void set(int i, float v);
		void set(int i, int j, float v);
		void set(float *v);
		void set(float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3);
		void set(Vector3 a, Vector3 b, Vector3 c);
		float operator[](int i);
		void operator=(Matrix3 M);
		void operator==(Matrix3 M);
		void operator+=(Matrix3 M);
		void operator+=(float v);
		void operator-=(Matrix3 M);
		void operator-=(float v);
		void operator*=(Matrix3 M);
		void operator*=(float v);
		void operator/=(float v);
		Matrix3 operator+(Matrix3 M);
		Matrix3 operator+(float v);
		Matrix3 operator-(Matrix3 M);
		Matrix3 operator-(float v);
		Matrix3 operator*(Matrix3 M);
		Vector3 operator*(Vector3 v);
		Matrix3 operator*(float v);
		Matrix3 operator/(float v);
	};

	class SPAMAPI Matrix4
	{
	public:
		float data[16];

		Matrix4();
		Matrix4(float v);
		Matrix4(float *v);
		Matrix4(float a1, float a2, float a3, float a4, float b1, float b2, float b3, float b4, float c1, float c2, float c3, float c4, float d1, float d2, float d3, float d4);
		Matrix4(Vector4 a, Vector4 b, Vector4 c, Vector4 d);
		~Matrix4();
		void copy(Spam::Matrix4 M);
		void copy(float *v);
		float *get();
		float get(int i);
		float get(int i, int j);
		void set(int i, float v);
		void set(int i, int j, float v);
		void set(float *v);
		void set(float a1, float a2, float a3, float a4, float b1, float b2, float b3, float b4, float c1, float c2, float c3, float c4, float d1, float d2, float d3, float d4);
		void set(Vector4 a, Vector4 b, Vector4 c, Vector4 d);
		float operator[](int i);
		void operator=(Matrix4 M);
		void operator==(Matrix4 M);
		void operator+=(Matrix4 M);
		void operator+=(float v);
		void operator-=(Matrix4 M);
		void operator-=(float v);
		void operator*=(Matrix4 M);
		void operator*=(float v);
		void operator/=(float v);
		Matrix4 operator+(Matrix4 M);
		Matrix4 operator+(float v);
		Matrix4 operator-(Matrix4 M);
		Matrix4 operator-(float v);
		Matrix4 operator*(Matrix4 M);
		Vector4 operator*(Vector4 v);
		Matrix4 operator*(float v);
		Matrix4 operator/(float v);
	};

	void SPAMAPI multiplyMatrix(float *out, float *m1, int w1, int h1, float *m2, int w2, int h2);

	void SPAMAPI translate(Matrix4 &M, float x, float y, float z);
	void SPAMAPI rotate(Matrix4 &M, float a, float x, float y, float z);
	void SPAMAPI scale(Matrix4 &M, float x, float y, float z);
	Matrix4 SPAMAPI ortho(float l, float r, float b, float t, float n, float f);
	Matrix4 SPAMAPI frustum(float l, float r, float b, float t, float n, float f);
	Matrix4 SPAMAPI inverse(Matrix4 M);
};