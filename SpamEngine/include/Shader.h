#pragma once

#include <fstream>
#include <string>
#include <GL\glew.h>
#include <GL\GL.h>
#include "dllmain.h"

#pragma comment(lib, "glew32")

namespace Spam
{
	class SPAMAPI Shader
	{
	public:
		Shader(const char *vertexShaderSrc, const char *fragmentShaderSrc);
		~Shader();
		void use();
		int getProgram();

		void uniform1i(const char *loc, int v);
		void uniform2i(const char *loc, int v1, int v2);
		void uniform3i(const char *loc, int v1, int v2, int v3);
		void uniform4i(const char *loc, int v1, int v2, int v3, int v4);
		void uniform2iv(const char *loc, int *v, int count = 1);
		void uniform3iv(const char *loc, int *v, int count = 1);
		void uniform4iv(const char *loc, int *v, int count = 1);

		void uniform1ui(const char *loc, unsigned int v);
		void uniform2ui(const char *loc, unsigned int v1, unsigned int v2);
		void uniform3ui(const char *loc, unsigned int v1, unsigned int v2, unsigned int v3);
		void uniform4ui(const char *loc, unsigned int v1, unsigned int v2, unsigned int v3, unsigned int v4);
		void uniform2uiv(const char *loc, unsigned int *v, int count = 1);
		void uniform3uiv(const char *loc, unsigned int *v, int count = 1);
		void uniform4uiv(const char *loc, unsigned int *v, int count = 1);

		void uniform1f(const char *loc, float v);
		void uniform2f(const char *loc, float v1, float v2);
		void uniform3f(const char *loc, float v1, float v2, float v3);
		void uniform4f(const char *loc, float v1, float v2, float v3, float v4);
		void uniform2fv(const char *loc, float *v, int count = 1);
		void uniform3fv(const char *loc, float *v, int count = 1);
		void uniform4fv(const char *loc, float *v, int count = 1);

		void uniform1d(const char *loc, double v);
		void uniform2d(const char *loc, double v1, double v2);
		void uniform3d(const char *loc, double v1, double v2, double v3);
		void uniform4d(const char *loc, double v1, double v2, double v3, double v4);
		void uniform2dv(const char *loc, double *v, int count = 1);
		void uniform3dv(const char *loc, double *v, int count = 1);
		void uniform4dv(const char *loc, double *v, int count = 1);

		void uniformMatrix2x3fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix2x4fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix3x2fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix3x4fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix4x2fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix4x3fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix2fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix3fv(const char *loc, bool transpose, float *v, int count = 1);
		void uniformMatrix4fv(const char *loc, bool transpose, float *v, int count = 1);

		void uniformMatrix2x3dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix2x4dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix3x2dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix3x4dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix4x2dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix4x3dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix2dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix3dv(const char *loc, bool transpose, double *v, int count = 1);
		void uniformMatrix4dv(const char *loc, bool transpose, double *v, int count = 1);

	private:
		GLuint program;

		std::string loadFromFile(std::string filename);
	};
};