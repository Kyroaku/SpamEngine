#pragma once

#include <Windows.h>
#include <stdio.h>
#include <GL\glew.h>
#include <GL\GL.h>
#include <FreeImage\FreeImage.h>
#include "dllmain.h"

#pragma comment(lib, "FreeImage")

namespace Spam
{
	class SPAMAPI Texture
	{
		unsigned int texture;
		FIBITMAP *bitmap;

		void create(const char *filename, int minFilter, int magFilter, int sWrap, int tWrap);

	public:
		Texture();
		Texture(const char *filename);
		Texture(const char *filename, int min, int mag);
		Texture(const char *filename, int min, int mag, int s, int t);
		~Texture();
		void setFilter(int min, int mag);
		void setWrapping(int s, int t);
		unsigned int getTexture();
		int getWidth();
		int getHeight();
		int getBpp();
	};
}