#pragma once

#include "Vector.h"
#include "Bone.h"
#include "Matrix.h"

namespace Spam
{
	class SPAMAPI Mesh
	{
	public:
		unsigned int numVertices;
		unsigned int numIndices;
		unsigned int numBones;
		Spam::Vector3 *vertexPositions, *vertexTextureCoords,
			*vertexNormals, *vertexTangents, *vertexBitangents;
		Spam::Vector4 *vertexColors;
		unsigned int *vertexIndices;
		unsigned int *boneIds;
		float *boneWeights;
		Spam::Bone **bones;
		Spam::Matrix4 *boneMatrices;
		unsigned int material;

		Mesh();
		~Mesh();
	};
};