#pragma once

#include "Matrix.h"
#include "Node.h"

namespace Spam
{
	class SPAMAPI Bone
	{
	public:
		Spam::Matrix4 bindposeMatrix;
		Spam::Node *node;

		Bone();
		~Bone();
	};
};