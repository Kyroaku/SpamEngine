#pragma once

#include <stdio.h>
#include <vector>
#include <Windows.h>
#include <GL\glew.h>
#include <GL\GL.h>
#include "dllmain.h"
#include "Macros.h"
#include "Window.h"
#include "VirtualClasses.h"
#include "Shader.h"
#include "Camera.h"
#include "Vector.h"
#include "Matrix.h"
#include "Texture.h"
#include "Scene.h"

#pragma comment(lib, "opengl32")
#pragma comment(lib, "glew32")

namespace Spam
{
	class SPAMAPI Engine
	{
		class SPAMAPI Fps
		{
		public:
			Fps() :
				fps(0),
				frame(0),
				timer(0.0f),
				lastFrameTime(0)
			{

			}
			int fps, frame;
			float timer;
			unsigned int lastFrameTime;

			void update(float dt)
			{
				frame++;
				timer += dt;
				if (timer >= 1.0f)
				{
					fps = frame;
					frame = 0;
					timer -= 1.0f;
				}
			}
		};

		Spam::Window *window;
		Spam::Renderer *renderer;
		Spam::Keyboard *keyboard;
		Spam::Mouse *mouse;
		Fps fps;
		bool bQuit;

		void init();

	public:
		Engine(const char *title);
		Engine(const char *title, int width, int height);
		Engine(const char *title, int width, int height, short bitsPerPixel, bool fullscreen);
		~Engine();
		void realTimeLoop();
		void setRenderer(Spam::Renderer *renderer);
		void setKeyboard(Spam::Keyboard *keyboard);
		void setMouse(Spam::Mouse *mouse);
		Spam::Renderer *getRenderer();
		Spam::Keyboard *getKeyboard();
		Spam::Mouse *getMouse();
		Spam::Window *getWindow();
		void enableRelativeMouseMode();
		void disableRelativeMouseMode();
		bool isRelativeMouseMode();
		int getFps();

		void quit();

		void bindTexture(Spam::Texture *texture, int i);

		void render(Spam::Scene *scene, Spam::Mesh *mesh, Spam::Material *material);
		void render(Spam::Scene *scene, Spam::Node *node, Spam::Shader *shader, Spam::Matrix4 transformation = Spam::Matrix4(1.0f));
		void render(Spam::Scene *scene, Spam::Shader *shader);
	};
}; //namespace Spam