#pragma once

#include <math.h>
#include "dllmain.h"
#include "Vector.h"
#include "Matrix.h"

namespace Spam
{
	//Camera
	class SPAMAPI Camera
	{
	public:
		Camera();
		virtual Spam::Matrix4 getMatrix() {
			return Spam::Matrix4(1.0f);
		}
	protected:
		Spam::Vector3 position, target, rotation;
	};

	//CameraFps
	class SPAMAPI CameraFps : public Camera
	{
	public:
		CameraFps();
		Spam::Matrix4 getMatrix();
		void setPosition(float x, float y, float z);
		void setRotation(float x, float y, float z);
		void move(float x, float y, float z);
		void moveForward(float s);
		void moveBackward(float s);
		void moveLeft(float s);
		void moveRight(float s);
		void moveUp(float s);
		void moveDown(float s);
		void rotate(float x, float y, float z);
	};
}