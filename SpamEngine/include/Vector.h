#pragma once

#include <math.h>
#include "dllmain.h"
#include "Matrix.h"

namespace Spam
{
	class Matrix2;
	class Matrix3;
	class Matrix4;
	class Vector2;
	class Vector3;
	class Vector4;

	class SPAMAPI Vector2
	{
	public:
		float x, y;

		Vector2();
		Vector2(float v);
		Vector2(float x, float y);
		Vector2(float v[2]);
		Vector2(Vector3 v);
		Vector2(Vector4 v);
		float *get();
		void set(float x, float y);
		void set(float v[2]);
		void operator=(Vector2 v);
		bool operator==(Vector2 v);
		void operator+=(Vector2 v);
		void operator+=(float v);
		void operator-=(Vector2 v);
		void operator-=(float v);
		void operator*=(Vector2 v);
		void operator*=(float v);
		void operator/=(float v);
		Vector2 operator+(Vector2 v);
		Vector2 operator+(float v);
		Vector2 operator-(Vector2 v);
		Vector2 operator-(float v);
		Vector2 operator*(Vector2 v);
		Vector2 operator*(float v);
		Vector2 operator/(float v);
		/* TODO implement multiplication with matrices */
	};

	class SPAMAPI Vector3
	{
	public:
		float x, y, z;

		Vector3();
		Vector3(float v);
		Vector3(float x, float y, float z);
		Vector3(float v[3]);
		Vector3(Vector2 v, float z);
		Vector3(Vector4 v);
		float *get();
		void set(float x, float y, float z);
		void set(float v[3]);
		void set(Spam::Vector2 v, float z);
		void operator=(Vector3 v);
		bool operator==(Vector3 v);
		void operator+=(Vector3 v);
		void operator+=(float v);
		void operator-=(Vector3 v);
		void operator-=(float v);
		void operator*=(Vector3 v);
		void operator*=(float v);
		void operator/=(float v);
		Vector3 operator+(Vector3 v);
		Vector3 operator+(float v);
		Vector3 operator-(Vector3 v);
		Vector3 operator-(float v);
		Vector3 operator*(Vector3 v);
		Vector3 operator*(float v);
		Vector3 operator*(Matrix3 M);
		Vector3 operator/(float v);
		/* TODO implement multiplication with matrices */
	};

	class SPAMAPI Vector4
	{
	public:
		float x, y, z, w;

		Vector4();
		Vector4(float v);
		Vector4(float x, float y, float z, float w);
		Vector4(float v[4]);
		Vector4(Vector2 v, float z, float w);
		Vector4(Vector3 v, float w);
		float *get();
		void set(float x, float y, float z, float w);
		void set(float x, float y, float z);
		void set(float v[4]);
		void set(Spam::Vector2 v, float z, float w);
		void set(Spam::Vector3 v, float w);
		void operator=(Vector4 v);
		bool operator==(Vector4 v);
		void operator+=(Vector4 v);
		void operator+=(float v);
		void operator-=(Vector4 v);
		void operator-=(float v);
		void operator*=(Vector4 v);
		void operator*=(float v);
		void operator/=(float v);
		Vector4 operator+(Vector4 v);
		Vector4 operator+(float v);
		Vector4 operator-(Vector4 v);
		Vector4 operator-(float v);
		Vector4 operator*(Vector4 v);
		Vector4 operator*(float v);
		Vector4 operator*(Matrix4 M);
		Vector4 operator/(float v);
	};

	float SPAMAPI length(Vector2 v);
	float SPAMAPI length(Vector3 v);
	float SPAMAPI length(Vector4 v);
	Spam::Vector2 SPAMAPI normalize(Spam::Vector2 v);
	Spam::Vector3 SPAMAPI normalize(Spam::Vector3 v);
	Spam::Vector4 SPAMAPI normalize(Spam::Vector4 v);
};