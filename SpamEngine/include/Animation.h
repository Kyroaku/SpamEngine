#pragma once

#include "AnimationChannel.h"

namespace Spam
{
	class SPAMAPI Animation
	{
	public:
		double duration;
		double ticksPerSecond;
		unsigned int numChannels;
		Spam::AnimationChannel **channels;

		Animation();
		~Animation();
	};
};