#include "Scene.h"

Spam::Scene::Scene() :
meshes(0),
numMeshes(0),
rootNode(0),
materials(0),
numMaterials(0),
animations(0),
numAnimations(0),
currentAnimation(0),
animationTime(0.0),
animating(false)
{

}
Spam::Scene::Scene(const char *path) :
meshes(0),
numMeshes(0),
rootNode(0)
{
	loadFromFile(path);
}
void Spam::Scene::copyNode(Spam::Node *node, Spam::Node *parent, aiNode *ainode)
{
	node->name = new char[ainode->mName.length + 1];
	strcpy_s(node->name, ainode->mName.length + 1, ainode->mName.C_Str());
	node->numMeshes = ainode->mNumMeshes;
	node->meshes = new unsigned int[ainode->mNumMeshes];
	loop(m_id, ainode->mNumMeshes)
		node->meshes[m_id] = ainode->mMeshes[m_id];
	node->numChildrens = ainode->mNumChildren;
	node->childrens = new Spam::Node*[ainode->mNumChildren];
	loop(c_id, ainode->mNumChildren)
		node->childrens[c_id] = new Spam::Node();
	node->parent = parent;
	node->transformation = (float*)&ainode->mTransformation.a1;

	/* print data */
#ifdef _PRINT_ASSIMP_SCENE_DATA
	printf("mName=\"%s\"\n", ainode->mName.C_Str());
	for (int y = 0; y < 4; y++)
	{
		printf("  ");
		for (int x = 0; x < 4; x++)
			printf("%f, ", node->transformation.get(x, y));
		printf("\n");
	}
#endif
	loop(child_id, ainode->mNumChildren)
	{
		copyNode(node->childrens[child_id], node, ainode->mChildren[child_id]);
	}
}
void Spam::Scene::loadFromFile(const char *path)
{
	Assimp::Importer importer;
	const aiScene *scene = importer.ReadFile(path, aiProcess_JoinIdenticalVertices);
	if (!scene) {
		printf("Loading file error: '%s'\n", path);
		return;
	}

	/* nodes */
	rootNode = new Spam::Node();
	copyNode(rootNode, 0, scene->mRootNode);

	/* meshes */
	meshes = new Spam::Mesh*[scene->mNumMeshes];
	numMeshes = scene->mNumMeshes;
	loop(m_id, scene->mNumMeshes)
	{
		meshes[m_id] = new Spam::Mesh();
		/* vertices */
		meshes[m_id]->numVertices = scene->mMeshes[m_id]->mNumVertices;
		if (scene->mMeshes[m_id]->HasPositions())
			meshes[m_id]->vertexPositions = new Spam::Vector3[scene->mMeshes[m_id]->mNumVertices];

		if (scene->mMeshes[m_id]->HasVertexColors(0))
			meshes[m_id]->vertexColors = new Spam::Vector4[scene->mMeshes[m_id]->mNumVertices];

		if (scene->mMeshes[m_id]->HasTextureCoords(0))
			meshes[m_id]->vertexTextureCoords = new Spam::Vector3[scene->mMeshes[m_id]->mNumVertices];

		if (scene->mMeshes[m_id]->HasNormals())
			meshes[m_id]->vertexNormals = new Spam::Vector3[scene->mMeshes[m_id]->mNumVertices];

		if (scene->mMeshes[m_id]->HasTangentsAndBitangents()) {
			meshes[m_id]->vertexTangents = new Spam::Vector3[scene->mMeshes[m_id]->mNumVertices];
			meshes[m_id]->vertexBitangents = new Spam::Vector3[scene->mMeshes[m_id]->mNumVertices];
		}

		loop(v_id, scene->mMeshes[m_id]->mNumVertices)
		{
			if (meshes[m_id]->vertexPositions != 0)
				meshes[m_id]->vertexPositions[v_id].set(
				scene->mMeshes[m_id]->mVertices[v_id].x, scene->mMeshes[m_id]->mVertices[v_id].y, scene->mMeshes[m_id]->mVertices[v_id].z);

			if (meshes[m_id]->vertexColors != 0)
				meshes[m_id]->vertexColors[v_id].set(
				scene->mMeshes[m_id]->mColors[0][v_id].r, scene->mMeshes[m_id]->mColors[0][v_id].g, scene->mMeshes[m_id]->mColors[0][v_id].b, scene->mMeshes[m_id]->mColors[0][v_id].a);

			if (meshes[m_id]->vertexTextureCoords != 0)
				meshes[m_id]->vertexTextureCoords[v_id].set(
				scene->mMeshes[m_id]->mTextureCoords[0][v_id].x, scene->mMeshes[m_id]->mTextureCoords[0][v_id].y, scene->mMeshes[m_id]->mTextureCoords[0][v_id].z);

			if (meshes[m_id]->vertexNormals != 0)
				meshes[m_id]->vertexNormals[v_id].set(
				scene->mMeshes[m_id]->mNormals[v_id].x, scene->mMeshes[m_id]->mNormals[v_id].y, scene->mMeshes[m_id]->mNormals[v_id].z);

			if (meshes[m_id]->vertexTangents != 0)
				meshes[m_id]->vertexTangents[v_id].set(
				scene->mMeshes[m_id]->mTangents[v_id].x, scene->mMeshes[m_id]->mTangents[v_id].y, scene->mMeshes[m_id]->mTangents[v_id].z);

			if (meshes[m_id]->vertexBitangents != 0)
				meshes[m_id]->vertexBitangents[v_id].set(
				scene->mMeshes[m_id]->mBitangents[v_id].x, scene->mMeshes[m_id]->mBitangents[v_id].y, scene->mMeshes[m_id]->mBitangents[v_id].z);
		}

		/* indices */
		if (scene->mMeshes[m_id]->HasFaces())
		{
			meshes[m_id]->numIndices = scene->mMeshes[m_id]->mNumFaces * 3;
			meshes[m_id]->vertexIndices = new unsigned int[scene->mMeshes[m_id]->mNumFaces * 3];

			loop(f_id, scene->mMeshes[m_id]->mNumFaces)
			{
				loop(i, 3)
					meshes[m_id]->vertexIndices[f_id * 3 + i] = scene->mMeshes[m_id]->mFaces[f_id].mIndices[i];
			}
		}

		/* material index */
		meshes[m_id]->material = scene->mMeshes[m_id]->mMaterialIndex;

		/* bones */
		if (scene->mMeshes[m_id]->HasBones()) {
			meshes[m_id]->numBones = scene->mMeshes[m_id]->mNumBones;
			meshes[m_id]->boneMatrices = new Spam::Matrix4[scene->mMeshes[m_id]->mNumBones];
			meshes[m_id]->bones = new Spam::Bone*[scene->mMeshes[m_id]->mNumBones];
			meshes[m_id]->boneIds = new unsigned int[scene->mMeshes[m_id]->mNumVertices * 4];
			meshes[m_id]->boneWeights = new float[scene->mMeshes[m_id]->mNumVertices * 4];
			loop(i, scene->mMeshes[m_id]->mNumVertices * 4) {
				meshes[m_id]->boneIds[i] = 0;
				meshes[m_id]->boneWeights[i] = 0.0f;
			}
		}
		loop(b_id, scene->mMeshes[m_id]->mNumBones)
		{
			meshes[m_id]->bones[b_id] = new Spam::Bone();
			meshes[m_id]->bones[b_id]->node = rootNode->findNode(scene->mMeshes[m_id]->mBones[b_id]->mName.C_Str());
			meshes[m_id]->bones[b_id]->bindposeMatrix = (float*)&scene->mMeshes[m_id]->mBones[b_id]->mOffsetMatrix.a1;
			loop(bw_id, scene->mMeshes[m_id]->mBones[b_id]->mNumWeights)
			{
				loop(i, 4) {
					if (meshes[m_id]->boneWeights[scene->mMeshes[m_id]->mBones[b_id]->mWeights[bw_id].mVertexId * 4 + i] == 0.0f) {
						meshes[m_id]->boneWeights[scene->mMeshes[m_id]->mBones[b_id]->mWeights[bw_id].mVertexId * 4 + i] = scene->mMeshes[m_id]->mBones[b_id]->mWeights[bw_id].mWeight;
						meshes[m_id]->boneIds[scene->mMeshes[m_id]->mBones[b_id]->mWeights[bw_id].mVertexId * 4 + i] = b_id;
						break;
					}
				}
			}
			meshes[m_id]->boneMatrices[b_id] = meshes[m_id]->bones[b_id]->node->getAbsoluteMatrix() * meshes[m_id]->bones[b_id]->bindposeMatrix;
		}
	}

	/* materials */
	materials = new Spam::Material*[scene->mNumMaterials];
	numMaterials = scene->mNumMaterials;
	loop(mat_id, scene->mNumMaterials)
	{
		materials[mat_id] = new Spam::Material();
		aiColor3D color(0.f, 0.f, 0.f);
		scene->mMaterials[mat_id]->Get(AI_MATKEY_COLOR_AMBIENT, color);
		materials[mat_id]->ambient.set(color.r, color.g, color.b);
		scene->mMaterials[mat_id]->Get(AI_MATKEY_COLOR_DIFFUSE, color);
		materials[mat_id]->diffuse.set(color.r, color.g, color.b);
		scene->mMaterials[mat_id]->Get(AI_MATKEY_COLOR_SPECULAR, color);
		materials[mat_id]->specular.set(color.r, color.g, color.b);
		scene->mMaterials[mat_id]->Get(AI_MATKEY_COLOR_EMISSIVE, color);
		materials[mat_id]->emissive.set(color.r, color.g, color.b);
		float shinin = 0.0f;
		scene->mMaterials[mat_id]->Get(AI_MATKEY_SHININESS, shinin);
		materials[mat_id]->shininess = shinin;
		aiString path;
		if (scene->mMaterials[mat_id]->GetTexture(aiTextureType_DIFFUSE, 0, &path) == AI_SUCCESS)
			materials[mat_id]->texture = new Spam::Texture(path.C_Str());
	}

	/* animations */
	animations = new Spam::Animation*[scene->mNumAnimations];
	numAnimations = scene->mNumAnimations;
	loop(a_id, scene->mNumAnimations)
	{
		animations[a_id] = new Spam::Animation();
		animations[a_id]->duration = scene->mAnimations[a_id]->mDuration;
		animations[a_id]->ticksPerSecond = scene->mAnimations[a_id]->mTicksPerSecond;
		animations[a_id]->channels = new Spam::AnimationChannel*[scene->mAnimations[a_id]->mNumChannels];
		animations[a_id]->numChannels = scene->mAnimations[a_id]->mNumChannels;
		loop(c_id, scene->mAnimations[a_id]->mNumChannels)
		{
			animations[a_id]->channels[c_id] = new Spam::AnimationChannel();
			animations[a_id]->channels[c_id]->node = rootNode->findNode(scene->mAnimations[a_id]->mChannels[c_id]->mNodeName.C_Str());
			animations[a_id]->channels[c_id]->keys = new Spam::ChannelFrameKey*[scene->mAnimations[a_id]->mChannels[c_id]->mNumPositionKeys];
			animations[a_id]->channels[c_id]->numKeys = scene->mAnimations[a_id]->mChannels[c_id]->mNumPositionKeys;
			loop(k_id, scene->mAnimations[a_id]->mChannels[c_id]->mNumPositionKeys)
			{
				animations[a_id]->channels[c_id]->keys[k_id] = new Spam::ChannelFrameKey();
				animations[a_id]->channels[c_id]->keys[k_id]->time = scene->mAnimations[a_id]->mChannels[c_id]->mPositionKeys[k_id].mTime;
				Spam::translate(animations[a_id]->channels[c_id]->keys[k_id]->transformation,
					scene->mAnimations[a_id]->mChannels[c_id]->mPositionKeys[k_id].mValue.x,
					scene->mAnimations[a_id]->mChannels[c_id]->mPositionKeys[k_id].mValue.y,
					scene->mAnimations[a_id]->mChannels[c_id]->mPositionKeys[k_id].mValue.z);
				Spam::Matrix4 quat;
				quat.data[0] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().a1;
				quat.data[1] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().a2;
				quat.data[2] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().a3;
				quat.data[3] = 0.0f;
				quat.data[4] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().b1;
				quat.data[5] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().b2;
				quat.data[6] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().b3;
				quat.data[7] = 0.0f;
				quat.data[8] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().c1;
				quat.data[9] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().c2;
				quat.data[10] = scene->mAnimations[a_id]->mChannels[c_id]->mRotationKeys[k_id].mValue.GetMatrix().c3;
				quat.data[11] = 0.0f;
				quat.data[12] = 0.0f;
				quat.data[13] = 0.0f;
				quat.data[14] = 0.0f;
				quat.data[15] = 1.0f;
				animations[a_id]->channels[c_id]->keys[k_id]->transformation *= quat;
				Spam::scale(animations[a_id]->channels[c_id]->keys[k_id]->transformation,
					scene->mAnimations[a_id]->mChannels[c_id]->mScalingKeys[k_id].mValue.x,
					scene->mAnimations[a_id]->mChannels[c_id]->mScalingKeys[k_id].mValue.y,
					scene->mAnimations[a_id]->mChannels[c_id]->mScalingKeys[k_id].mValue.z);
			}
		}
	}
}
void Spam::Scene::update(double dt)
{
	if (!animating || numAnimations == 0)
		return;

	animationTime += dt;
	if (animationTime >= animations[currentAnimation]->duration)
		animationTime = 0.0f;

	loop(c_id, animations[currentAnimation]->numChannels)
	{
		unsigned int keyId = -1;

		loop(k_id, animations[currentAnimation]->channels[c_id]->numKeys)
		{
			if (animations[currentAnimation]->channels[c_id]->keys[k_id]->time >= animationTime) {
				keyId = k_id;
				break;
			}
		}

		if (keyId == -1)
			keyId = animations[currentAnimation]->channels[c_id]->numKeys - 1;

		animations[currentAnimation]->channels[c_id]->node->transformation = animations[currentAnimation]->channels[c_id]->keys[keyId]->transformation;
	}
}
void Spam::Scene::startAnimation()
{
	animating = true;
}
void Spam::Scene::stopAnimation()
{
	animating = false;
}
bool Spam::Scene::isAnimating()
{
	return animating;
}
void Spam::Scene::setAnimationTime(double time)
{
	animationTime = time * animations[currentAnimation]->duration;
}
void Spam::Scene::setAnimation(unsigned int animation_id)
{
	currentAnimation = animation_id % numAnimations;
}