#include "Mesh.h"

Spam::Mesh::Mesh() :
vertexPositions(0),
vertexColors(0),
vertexTextureCoords(0),
vertexNormals(0),
vertexTangents(0),
vertexBitangents(0),
numVertices(0),
vertexIndices(0),
numIndices(0),
boneIds(0),
boneWeights(0),
bones(0),
boneMatrices(0)
{

}