#include "Node.h"

Spam::Node::Node() :
meshes(0),
numMeshes(0),
childrens(0),
numChildrens(0),
parent(0),
transformation(1.0f)
{

}
Spam::Node *Spam::Node::findNode(const char *name)
{
	if (strcmp(this->name, name) == 0)
		return this;
	loop(i, this->numChildrens)
	{
		Spam::Node *temp = this->childrens[i]->findNode(name);
		if (temp != 0)
			return temp;
	}
	return 0;
}
Spam::Matrix4 Spam::Node::getAbsoluteMatrix()
{
	if (this->parent == 0)
		return this->transformation;
	else
		return this->parent->getAbsoluteMatrix() * this->transformation;
}