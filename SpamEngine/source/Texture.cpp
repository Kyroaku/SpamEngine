#include "Texture.h"

Spam::Texture::Texture() :
texture(0),
bitmap(0)
{

}
Spam::Texture::Texture(const char *filename)
{
	create(filename, GL_LINEAR, GL_LINEAR, GL_REPEAT, GL_REPEAT);
}
Spam::Texture::Texture(const char *filename, int min, int mag)
{
	create(filename, min, mag, GL_REPEAT, GL_REPEAT);
}
Spam::Texture::Texture(const char *filename, int min, int mag, int s, int t)
{
	create(filename, min, mag, s, t);
}
Spam::Texture::~Texture()
{
	FreeImage_Unload(bitmap);
	glDeleteTextures(1, &texture);
}
void Spam::Texture::create(const char *filename, int minFilter, int magFilter, int sWrap, int tWrap)
{
	FREE_IMAGE_FORMAT fif = FreeImage_GetFIFFromFilename(filename);
	bitmap = FreeImage_Load(fif, filename, 0);
	if (bitmap)
	{
		glEnable(GL_TEXTURE_2D);
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, magFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, minFilter);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sWrap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, tWrap);
		int width = FreeImage_GetWidth(bitmap);
		int height = FreeImage_GetHeight(bitmap);
		int bpp = FreeImage_GetBPP(bitmap);
		unsigned char *pixelData = FreeImage_GetBits(bitmap);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, bpp == 32 ? GL_BGRA : bpp == 24 ? GL_BGR : GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, pixelData);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		printf("Can't open file: '%s'\n", filename);
	}
}
void Spam::Texture::setFilter(int min, int mag)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, mag);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, min);
}
void Spam::Texture::setWrapping(int s, int t)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, s);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, t);
}
unsigned int Spam::Texture::getTexture()
{
	return texture;
}
int Spam::Texture::getWidth()
{
	return FreeImage_GetWidth(bitmap);
}
int Spam::Texture::getHeight()
{
	return FreeImage_GetHeight(bitmap);
}
int Spam::Texture::getBpp()
{
	return FreeImage_GetBPP(bitmap);
}