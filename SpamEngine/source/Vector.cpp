#include "Vector.h"

Spam::Vector2::Vector2()
{
	x = y = 0.0f;
}
Spam::Vector2::Vector2(float v)
{
	set(v, v);
}
Spam::Vector2::Vector2(float x, float y)
{
	set(x, y);
}
Spam::Vector2::Vector2(float v[2])
{
	set(v);
}
Spam::Vector2::Vector2(Vector3 v)
{
	set(v.x, v.y);
}
Spam::Vector2::Vector2(Vector4 v)
{
	set(v.x, v.y);
}
float *Spam::Vector2::get()
{
	return new float[2] { x, y };
}
void Spam::Vector2::set(float x, float y)
{
	this->x = x;
	this->y = y;
}
void Spam::Vector2::set(float v[2])
{
	x = v[0];
	y = v[1];
}
void Spam::Vector2::operator=(Vector2 v)
{
	x = v.x;
	y = v.y;
}
bool Spam::Vector2::operator==(Vector2 v)
{
	if (x == v.x && y == v.y)
		return true;
	else
		return false;
}
void Spam::Vector2::operator+=(Vector2 v)
{
	x += v.x;
	y += v.y;
}
void Spam::Vector2::operator+=(float v)
{
	x += v;
	y += v;
}
void Spam::Vector2::operator-=(Vector2 v)
{
	x -= v.x;
	y -= v.y;
}
void Spam::Vector2::operator-=(float v)
{
	x -= v;
	y -= v;
}
void Spam::Vector2::operator*=(float v)
{
	x *= v;
	y *= v;
}
void Spam::Vector2::operator/=(float v)
{
	x /= v;
	y /= v;
}
Spam::Vector2 Spam::Vector2::operator+(Vector2 v)
{
	return Spam::Vector2(x + v.x, y + v.y);
}
Spam::Vector2 Spam::Vector2::operator+(float v)
{
	return Spam::Vector2(x + v, y + v);
}
Spam::Vector2 Spam::Vector2::operator-(Vector2 v)
{
	return Spam::Vector2(x - v.x, y - v.y);
}
Spam::Vector2 Spam::Vector2::operator-(float v)
{
	return Spam::Vector2(x - v, y - v);
}
Spam::Vector2 Spam::Vector2::operator*(float v)
{
	return Vector2(x*v, y*v);
}
Spam::Vector2 Spam::Vector2::operator/(float v)
{
	return Vector2(x / v, y / v);
}

Spam::Vector3::Vector3()
{
	set(0.f, 0.f, 0.f);
}
Spam::Vector3::Vector3(float v)
{
	set(v, v, v);
}
Spam::Vector3::Vector3(float x, float y, float z)
{
	set(x, y, z);
}
Spam::Vector3::Vector3(float v[3])
{
	set(v);
}
Spam::Vector3::Vector3(Vector2 v, float z)
{
	set(v, z);
}
Spam::Vector3::Vector3(Vector4 v)
{
	set(v.x, v.y, v.z);
}
float *Spam::Vector3::get()
{
	return new float[3] { x, y, z };
}
void Spam::Vector3::set(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}
void Spam::Vector3::set(float v[3])
{
	x = v[0];
	y = v[1];
	z = v[2];
}
void Spam::Vector3::set(Vector2 v, float z)
{
	this->x = v.x;
	this->y = v.y;
	this->z = z;
}
void Spam::Vector3::operator=(Vector3 v)
{
	x = v.x;
	y = v.y;
	z = v.z;
}
bool Spam::Vector3::operator==(Vector3 v)
{
	if (x == v.x && y == v.y && z == v.z)
		return true;
	else
		return false;
}
void Spam::Vector3::operator+=(Vector3 v)
{
	x += v.x;
	y += v.y;
	z += v.z;
}
void Spam::Vector3::operator+=(float v)
{
	x += v;
	y += v;
	z += v;
}
void Spam::Vector3::operator-=(Vector3 v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
}
void Spam::Vector3::operator-=(float v)
{
	x -= v;
	y -= v;
	z -= v;
}
void Spam::Vector3::operator*=(Vector3 v)
{
	Vector3 v2(x, y, z);
	x = v2.y*v.z - v2.z*v.y;
	y = v2.z*v.x - v2.x*v.z;
	z = v2.x*v.y - v2.y*v.x;
}
void Spam::Vector3::operator*=(float v)
{
	x *= v;
	y *= v;
	z *= v;
}
void Spam::Vector3::operator/=(float v)
{
	x /= v;
	y /= v;
	z /= v;
}
Spam::Vector3 Spam::Vector3::operator+(Vector3 v)
{
	return Spam::Vector3(x + v.x, y + v.y, z + v.z);
}
Spam::Vector3 Spam::Vector3::operator+(float v)
{
	return Spam::Vector3(x + v, y + v, z + v);
}
Spam::Vector3 Spam::Vector3::operator-(Vector3 v)
{
	return Spam::Vector3(x - v.x, y - v.y, z - v.z);
}
Spam::Vector3 Spam::Vector3::operator-(float v)
{
	return Spam::Vector3(x - v, y - v, z - v);
}
Spam::Vector3 Spam::Vector3::operator*(Vector3 v)
{
	return Vector3(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
}
Spam::Vector3 Spam::Vector3::operator*(float v)
{
	return Vector3(x*v, y*v, z*v);
}
Spam::Vector3 Spam::Vector3::operator*(Matrix3 M)
{
	return Vector3(
		x*M.data[0] + y*M.data[3] + z*M.data[6],
		x*M.data[1] + y*M.data[4] + z*M.data[7],
		x*M.data[2] + y*M.data[5] + z*M.data[8]
		);
}
Spam::Vector3 Spam::Vector3::operator/(float v)
{
	return Vector3(x / v, y / v, z / v);
}

Spam::Vector4::Vector4()
{
	set(0.0f, 0.0f, 0.0f, 1.0f);
}
Spam::Vector4::Vector4(float v)
{
	set(v, v, v, 1.f);
}
Spam::Vector4::Vector4(float x, float y, float z, float w)
{
	set(x, y, z, w);
}
Spam::Vector4::Vector4(float v[4])
{
	set(v);
}
Spam::Vector4::Vector4(Vector2 v, float z, float w)
{
	set(v, z, w);
}
Spam::Vector4::Vector4(Vector3 v, float w)
{
	set(v, w);
}
float *Spam::Vector4::get()
{
	return new float[4] { x, y, z, w };
}
void Spam::Vector4::set(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}
void Spam::Vector4::set(float x, float y, float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}
void Spam::Vector4::set(float v[4])
{
	x = v[0];
	y = v[1];
	z = v[2];
	w = v[3];
}
void Spam::Vector4::set(Vector2 v, float z, float w)
{
	this->x = v.x;
	this->y = v.y;
	this->z = z;
	this->w = w;
}
void Spam::Vector4::set(Vector3 v, float w)
{
	this->x = v.x;
	this->y = v.y;
	this->z = v.z;
	this->w = w;
}
void Spam::Vector4::operator=(Vector4 v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	w = v.w;
}
bool Spam::Vector4::operator==(Vector4 v)
{
	if (x == v.x && y == v.y && z == v.z && w == v.w)
		return true;
	else
		return false;
}
void Spam::Vector4::operator+=(Vector4 v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	w += v.w;
}
void Spam::Vector4::operator+=(float v)
{
	x += v;
	y += v;
	z += v;
	w += v;
}
void Spam::Vector4::operator-=(Vector4 v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	w -= v.w;
}
void Spam::Vector4::operator-=(float v)
{
	x -= v;
	y -= v;
	z -= v;
	w -= v;
}
void Spam::Vector4::operator*=(Vector4 v)
{
	Vector4 v2(x, y, z, w);
	x = v2.y*v.z - v2.z*v.y;
	y = v2.z*v.x - v2.x*v.z;
	z = v2.x*v.y - v2.y*v.x;
}
void Spam::Vector4::operator*=(float v)
{
	x *= v;
	y *= v;
	z *= v;
	w *= v;
}
void Spam::Vector4::operator/=(float v)
{
	x /= v;
	y /= v;
	z /= v;
	w /= v;
}
Spam::Vector4 Spam::Vector4::operator+(Vector4 v)
{
	return Spam::Vector4(x + v.x, y + v.y, z + v.z, w + v.w);
}
Spam::Vector4 Spam::Vector4::operator+(float v)
{
	return Spam::Vector4(x + v, y + v, z + v, w + v);
}
Spam::Vector4 Spam::Vector4::operator-(Vector4 v)
{
	return Spam::Vector4(x - v.x, y - v.y, z - v.z, w - v.w);
}
Spam::Vector4 Spam::Vector4::operator-(float v)
{
	return Spam::Vector4(x - v, y - v, z - v, w - v);
}
Spam::Vector4 Spam::Vector4::operator*(Vector4 v)
{
	return Vector4(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x, 1.f);
}
Spam::Vector4 Spam::Vector4::operator*(float v)
{
	return Vector4(x*v, y*v, z*v, w*v);
}
Spam::Vector4 Spam::Vector4::operator*(Matrix4 M)
{
	return Vector4(x*M.data[0] + y*M.data[4] + z*M.data[8] + w*M.data[12],
		x*M.data[1] + y*M.data[5] + z*M.data[9] + w*M.data[13],
		x*M.data[2] + y*M.data[6] + z*M.data[10] + w*M.data[14],
		x*M.data[3] + y*M.data[7] + z*M.data[11] + w*M.data[15]);
}
Spam::Vector4 Spam::Vector4::operator/(float v)
{
	return Vector4(x/v, y/v, z/v, w/v);
}

float Spam::length(Vector2 v)
{
	return sqrt(v.x*v.x + v.y*v.y);
}
float Spam::length(Vector3 v)
{
	return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}
float Spam::length(Vector4 v)
{
	return sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

Spam::Vector2 Spam::normalize(Spam::Vector2 v)
{
	return v / length(v);
}
Spam::Vector3 Spam::normalize(Spam::Vector3 v)
{
	return v / length(v);
}
Spam::Vector4 Spam::normalize(Spam::Vector4 v)
{
	return v / length(v);
}