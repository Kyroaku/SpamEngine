#include "Matrix.h"

Spam::Matrix2::Matrix2()
{
	data[0] = 1.f; data[1] = 0.f;
	data[2] = 0.f; data[3] = 1.f;
}
Spam::Matrix2::Matrix2(float v)
{
	data[0] = v; data[1] = 0.f;
	data[2] = 0.f; data[3] = v;
}
Spam::Matrix2::Matrix2(float *v)
{
	data[0] = v[0]; data[1] = v[1];
	data[2] = v[2]; data[3] = v[3];
}
Spam::Matrix2::Matrix2(float a1, float a2, float b1, float b2)
{
	set(a1, a2, b1, b2);
}
Spam::Matrix2::Matrix2(Vector2 a, Vector2 b)
{
	set(a, b);
}
Spam::Matrix2::~Matrix2()
{

}
void Spam::Matrix2::copy(Spam::Matrix2 M)
{
	data[0] = M.data[0]; data[1] = M.data[1];
	data[2] = M.data[2]; data[3] = M.data[3];
}
float* Spam::Matrix2::get()
{
	return data;
}
float Spam::Matrix2::get(int i)
{
	return data[i];
}
float Spam::Matrix2::get(int i, int j)
{
	return data[j * 3 + i];
}
void Spam::Matrix2::set(float *v)
{
	data[0] = v[0]; data[1] = v[1];
	data[2] = v[2]; data[3] = v[3];
}
void Spam::Matrix2::set(float a1, float a2, float b1, float b2)
{
	data[0] = a1; data[1] = a2;
	data[2] = b1; data[3] = b2;
}
void Spam::Matrix2::set(Vector2 a, Vector2 b)
{
	data[0] = a.x; data[1] = a.y;
	data[2] = b.x; data[3] = b.y;
}
void Spam::Matrix2::set(int i, float v)
{
	data[i] = v;
}
void Spam::Matrix2::set(int i, int j, float v)
{
	data[j * 3 + i] = v;
}
float Spam::Matrix2::operator[](int i)
{
	return data[i];
}
void Spam::Matrix2::operator=(Matrix2 M)
{
	copy(M);
}
void Spam::Matrix2::operator+=(Matrix2 M)
{
	data[0] += M.data[0]; data[1] += M.data[1];
	data[2] += M.data[2]; data[3] += M.data[3];
}
void Spam::Matrix2::operator+=(float v)
{
	data[0] += v; data[1] += v;
	data[2] += v; data[3] += v;
}
void Spam::Matrix2::operator-=(Matrix2 M)
{
	data[0] -= M.data[0]; data[1] -= M.data[1];
	data[2] -= M.data[2]; data[3] -= M.data[3];
}
void Spam::Matrix2::operator-=(float v)
{
	data[0] -= v; data[1] -= v;
	data[2] -= v; data[3] -= v;
}
void Spam::Matrix2::operator*=(Matrix2 M)
{
	Matrix2 M2(data);
	multiplyMatrix(data, M2.data, 2, 2, M.data, 2, 2);
}
void Spam::Matrix2::operator*=(float v)
{
	data[0] *= v; data[1] *= v;
	data[2] *= v; data[3] *= v;
}
void Spam::Matrix2::operator/=(float v)
{
	data[0] /= v; data[1] /= v;
	data[2] /= v; data[3] /= v;
}
Spam::Matrix2 Spam::Matrix2::operator+(Matrix2 M)
{
	return Matrix2(
		data[0] + M.data[0], data[1] + M.data[1],
		data[2] + M.data[2], data[3] + M.data[3]
		);
}
Spam::Matrix2 Spam::Matrix2::operator+(float v)
{
	return Matrix2(
		data[0] + v, data[1] + v,
		data[2] + v, data[3] + v
		);
}
Spam::Matrix2 Spam::Matrix2::operator-(Matrix2 M)
{
	return Matrix2(
		data[0] - M.data[0], data[1] - M.data[1],
		data[2] - M.data[2], data[3] - M.data[3]
		);
}
Spam::Matrix2 Spam::Matrix2::operator-(float v)
{
	return Matrix2(
		data[0] - v, data[1] - v,
		data[2] - v, data[3] - v
		);
}
Spam::Matrix2 Spam::Matrix2::operator*(Matrix2 M)
{
	Matrix2 M2;
	multiplyMatrix(M2.data, data, 2, 2, M.data, 2, 2);
	return M2;
}
Spam::Vector2 Spam::Matrix2::operator*(Vector2 v)
{
	float out[2];
	multiplyMatrix(out, data, 2, 2, &v.x, 1, 2);
	return Spam::Vector2(out);
}
Spam::Matrix2 Spam::Matrix2::operator*(float v)
{
	return Matrix2(
		data[0] * v, data[1] * v,
		data[2] * v, data[3] * v
		);
}
Spam::Matrix2 Spam::Matrix2::operator/(float v)
{
	return Matrix2(
		data[0] / v, data[1] / v,
		data[2] / v, data[3] / v
		);
}

Spam::Matrix3::Matrix3()
{
	data[0] = 1.f; data[1] = 0.f; data[2] = 0.f;
	data[3] = 0.f; data[4] = 1.f; data[5] = 0.f;
	data[6] = 0.f; data[7] = 0.f; data[8] = 1.f;
}
Spam::Matrix3::Matrix3(float v)
{
	data[0] = v; data[1] = 0.f; data[2] = 0.f;
	data[3] = 0.f; data[4] = v; data[5] = 0.f;
	data[6] = 0.f; data[7] = 0.f; data[8] = v;
}
Spam::Matrix3::Matrix3(float *v)
{
	data[0] = v[0]; data[1] = v[1]; data[2] = v[2];
	data[3] = v[3]; data[4] = v[4]; data[5] = v[5];
	data[6] = v[6]; data[7] = v[7]; data[8] = v[8];
}
Spam::Matrix3::Matrix3(float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3)
{
	set(a1, a2, a3, b1, b2, b3, c1, c2, c3);
}
Spam::Matrix3::Matrix3(Vector3 a, Vector3 b, Vector3 c)
{
	set(a, b, c);
}
Spam::Matrix3::~Matrix3()
{

}
void Spam::Matrix3::copy(Spam::Matrix3 M)
{
	data[0] = M.data[0]; data[1] = M.data[1]; data[2] = M.data[2];
	data[3] = M.data[3]; data[4] = M.data[4]; data[5] = M.data[5];
	data[6] = M.data[6]; data[7] = M.data[7]; data[8] = M.data[8];
}
float* Spam::Matrix3::get()
{
	return data;
}
float Spam::Matrix3::get(int i)
{
	return data[i];
}
float Spam::Matrix3::get(int i, int j)
{
	return data[j * 3 + i];
}
void Spam::Matrix3::set(float *v)
{
	data[0] = v[0]; data[1] = v[1]; data[2] = v[2];
	data[3] = v[3]; data[4] = v[4]; data[5] = v[5];
	data[6] = v[6]; data[7] = v[7]; data[8] = v[8];
}
void Spam::Matrix3::set(float a1, float a2, float a3, float b1, float b2, float b3, float c1, float c2, float c3)
{
	data[0] = a1; data[1] = a2; data[2] = a3;
	data[3] = b1; data[4] = b2; data[5] = b3;
	data[6] = c1; data[7] = c2; data[8] = c3;
}
void Spam::Matrix3::set(Vector3 a, Vector3 b, Vector3 c)
{
	data[0] = a.x; data[1] = a.y; data[2] = a.z;
	data[3] = b.x; data[4] = b.y; data[5] = b.z;
	data[6] = c.x; data[7] = c.y; data[8] = c.z;
}
void Spam::Matrix3::set(int i, float v)
{
	data[i] = v;
}
void Spam::Matrix3::set(int i, int j, float v)
{
	data[j * 3 + i] = v;
}
float Spam::Matrix3::operator[](int i)
{
	return data[i];
}
void Spam::Matrix3::operator=(Matrix3 M)
{
	copy(M);
}
void Spam::Matrix3::operator+=(Matrix3 M)
{
	data[0] += M.data[0]; data[1] += M.data[1]; data[2] += M.data[2];
	data[3] += M.data[3]; data[4] += M.data[4]; data[5] += M.data[5];
	data[6] += M.data[6]; data[7] += M.data[7]; data[8] += M.data[8];
}
void Spam::Matrix3::operator+=(float v)
{
	data[0] += v; data[1] += v; data[2] += v;
	data[3] += v; data[4] += v; data[5] += v;
	data[6] += v; data[7] += v; data[8] += v;
}
void Spam::Matrix3::operator-=(Matrix3 M)
{
	data[0] -= M.data[0]; data[1] -= M.data[1]; data[2] -= M.data[2];
	data[3] -= M.data[3]; data[4] -= M.data[4]; data[5] -= M.data[5];
	data[6] -= M.data[6]; data[7] -= M.data[7]; data[8] -= M.data[8];
}
void Spam::Matrix3::operator-=(float v)
{
	data[0] -= v; data[1] -= v; data[2] -= v;
	data[3] -= v; data[4] -= v; data[5] -= v;
	data[6] -= v; data[7] -= v; data[8] -= v;
}
void Spam::Matrix3::operator*=(Matrix3 M)
{
	Matrix3 M2(data);
	multiplyMatrix(data, M2.data, 3, 3, M.data, 3, 3);
}
void Spam::Matrix3::operator*=(float v)
{
	data[0] *= v; data[1] *= v; data[2] *= v;
	data[3] *= v; data[4] *= v; data[5] *= v;
	data[6] *= v; data[7] *= v; data[8] *= v;
}
void Spam::Matrix3::operator/=(float v)
{
	data[0] /= v; data[1] /= v; data[2] /= v;
	data[3] /= v; data[4] /= v; data[5] /= v;
	data[6] /= v; data[7] /= v; data[8] /= v;
}
Spam::Matrix3 Spam::Matrix3::operator+(Matrix3 M)
{
	return Matrix3(
		data[0] + M.data[0], data[1] + M.data[1], data[2] + M.data[2],
		data[3] + M.data[3], data[4] + M.data[4], data[5] + M.data[5],
		data[6] + M.data[6], data[7] + M.data[7], data[8] + M.data[8]
		);
}
Spam::Matrix3 Spam::Matrix3::operator+(float v)
{
	return Matrix3(
		data[0] + v, data[1] + v, data[2] + v,
		data[3] + v, data[4] + v, data[5] + v,
		data[6] + v, data[7] + v, data[8] + v
		);
}
Spam::Matrix3 Spam::Matrix3::operator-(Matrix3 M)
{
	return Matrix3(
		data[0] - M.data[0], data[1] - M.data[1], data[2] - M.data[2],
		data[3] - M.data[3], data[4] - M.data[4], data[5] - M.data[5],
		data[6] - M.data[6], data[7] - M.data[7], data[8] - M.data[8]
		);
}
Spam::Matrix3 Spam::Matrix3::operator-(float v)
{
	return Matrix3(
		data[0] - v, data[1] - v, data[2] - v,
		data[3] - v, data[4] - v, data[5] - v,
		data[6] - v, data[7] - v, data[8] - v
		);
}
Spam::Matrix3 Spam::Matrix3::operator*(Matrix3 M)
{
	Matrix3 M2;
	multiplyMatrix(M2.data, data, 3, 3, M.data, 3, 3);
	return M2;
}
Spam::Vector3 Spam::Matrix3::operator*(Vector3 v)
{
	float out[3];
	multiplyMatrix(out, data, 3, 3, &v.x, 1, 3);
	return Spam::Vector3(out);
}
Spam::Matrix3 Spam::Matrix3::operator*(float v)
{
	return Matrix3(
		data[0] * v, data[1] * v, data[2] * v,
		data[3] * v, data[4] * v, data[5] * v,
		data[6] * v, data[7] * v, data[8] * v
		);
}
Spam::Matrix3 Spam::Matrix3::operator/(float v)
{
	return Matrix3(
		data[0] / v, data[1] / v, data[2] / v,
		data[3] / v, data[4] / v, data[5] / v,
		data[6] / v, data[7] / v, data[8] / v
		);
}

Spam::Matrix4::Matrix4()
{
	for (int i = 0; i < 16; i++)
		data[i] = 0.0f;
	data[0] = data[5] = data[10] = data[15] = 1.0f;
}
Spam::Matrix4::Matrix4(float v)
{
	for (int i = 0; i < 16; i++)
		data[i] = 0;
	data[0] = data[5] = data[10] = v;
	data[15] = 1.0f;
}
Spam::Matrix4::Matrix4(float *v)
{
	for (int i = 0; i < 16; i++)
		data[i] = v[i];
}
Spam::Matrix4::Matrix4(float a1, float a2, float a3, float a4, float b1, float b2, float b3, float b4, float c1, float c2, float c3, float c4, float d1, float d2, float d3, float d4)
{
	set(a1, a2, a3, a4, b1, b2, b3, b4, c1, c2, c3, c4, d1, d2, d3, d4);
}
Spam::Matrix4::Matrix4(Vector4 a, Vector4 b, Vector4 c, Vector4 d)
{
	set(a, b, c, d);
}
Spam::Matrix4::~Matrix4()
{

}
void Spam::Matrix4::copy(Spam::Matrix4 M)
{
	for (int i = 0; i < 16; i++)
		data[i] = M.data[i];
}
void Spam::Matrix4::copy(float *v)
{
	for (int i = 0; i < 16; i++)
		data[i] = v[i];
}
float* Spam::Matrix4::get()
{
	return data;
}
float Spam::Matrix4::get(int i)
{
	return data[i];
}
float Spam::Matrix4::get(int i, int j)
{
	return data[j * 4 + i];
}
void Spam::Matrix4::set(float *v)
{
	for (int i = 0; i < 16; i++)
		data[i] = v[i];
}
void Spam::Matrix4::set(float a1, float a2, float a3, float a4, float b1, float b2, float b3, float b4, float c1, float c2, float c3, float c4, float d1, float d2, float d3, float d4)
{
	data[0] = a1; data[1] = a2; data[2] = a3; data[3] = a4;
	data[4] = b1; data[5] = b2; data[6] = b3; data[7] = b4;
	data[8] = c1; data[9] = c2; data[10] = c3; data[11] = c4;
	data[12] = d1; data[13] = d2; data[14] = d3; data[15] = d4;
}
void Spam::Matrix4::set(Vector4 a, Vector4 b, Vector4 c, Vector4 d)
{
	data[0] = a.x; data[1] = a.y; data[2] = a.z; data[3] = a.w;
	data[4] = b.x; data[5] = b.y; data[6] = b.z; data[7] = b.w;
	data[8] = c.x; data[9] = c.y; data[10] = c.z; data[11] = c.w;
	data[12] = d.x; data[13] = d.y; data[14] = d.z; data[15] = d.w;
}
void Spam::Matrix4::set(int i, float v)
{
	data[i] = v;
}
void Spam::Matrix4::set(int i, int j, float v)
{
	data[j * 4 + i] = v;
}
float Spam::Matrix4::operator[](int i)
{
	return data[i];
}
void Spam::Matrix4::operator=(Matrix4 M)
{
	copy(M);
}
void Spam::Matrix4::operator+=(Matrix4 M)
{
	for (int i = 0; i < 16; i++)
		data[i] += M.data[i];
}
void Spam::Matrix4::operator+=(float v)
{
	for (int i = 0; i < 16; i++)
		data[i] += v;
}
void Spam::Matrix4::operator-=(Matrix4 M)
{
	for (int i = 0; i < 16; i++)
		data[i] -= M.data[i];
}
void Spam::Matrix4::operator-=(float v)
{
	for (int i = 0; i < 16; i++)
		data[i] -= v;
}
void Spam::Matrix4::operator*=(Matrix4 M)
{
	Matrix4 M2(data);
	multiplyMatrix(data, M2.data, 4, 4, M.data, 4, 4);
}
void Spam::Matrix4::operator*=(float v)
{
	for (int i = 0; i < 16; i++)
		data[i] *= v;
}
void Spam::Matrix4::operator/=(float v)
{
	for (int i = 0; i < 16; i++)
		data[i] /= v;
}
Spam::Matrix4 Spam::Matrix4::operator+(Matrix4 M)
{
	return Matrix4(
		data[0] + M.data[0],
		data[1] + M.data[1],
		data[2] + M.data[2],
		data[3] + M.data[3],
		data[4] + M.data[4],
		data[5] + M.data[5],
		data[6] + M.data[6],
		data[7] + M.data[7],
		data[8] + M.data[8],
		data[9] + M.data[9],
		data[10] + M.data[10],
		data[11] + M.data[11],
		data[12] + M.data[12],
		data[13] + M.data[13],
		data[14] + M.data[14],
		data[15] + M.data[15]
		);
}
Spam::Matrix4 Spam::Matrix4::operator+(float v)
{
	return Matrix4(
		data[0] + v,
		data[1] + v,
		data[2] + v,
		data[3] + v,
		data[4] + v,
		data[5] + v,
		data[6] + v,
		data[7] + v,
		data[8] + v,
		data[9] + v,
		data[10] + v,
		data[11] + v,
		data[12] + v,
		data[13] + v,
		data[14] + v,
		data[15] + v
		);
}
Spam::Matrix4 Spam::Matrix4::operator-(Matrix4 M)
{
	return Matrix4(
		data[0] - M.data[0],
		data[1] - M.data[1],
		data[2] - M.data[2],
		data[3] - M.data[3],
		data[4] - M.data[4],
		data[5] - M.data[5],
		data[6] - M.data[6],
		data[7] - M.data[7],
		data[8] - M.data[8],
		data[9] - M.data[9],
		data[10] - M.data[10],
		data[11] - M.data[11],
		data[12] - M.data[12],
		data[13] - M.data[13],
		data[14] - M.data[14],
		data[15] - M.data[15]
		);
}
Spam::Matrix4 Spam::Matrix4::operator-(float v)
{
	return Matrix4(
		data[0] - v,
		data[1] - v,
		data[2] - v,
		data[3] - v,
		data[4] - v,
		data[5] - v,
		data[6] - v,
		data[7] - v,
		data[8] - v,
		data[9] - v,
		data[10] - v,
		data[11] - v,
		data[12] - v,
		data[13] - v,
		data[14] - v,
		data[15] - v
		);
}
Spam::Matrix4 Spam::Matrix4::operator*(Matrix4 M)
{
	Matrix4 M2;
	multiplyMatrix(M2.data, data, 4, 4, M.data, 4, 4);
	return M2;
}
Spam::Vector4 Spam::Matrix4::operator*(Vector4 v)
{
	float out[4];
	multiplyMatrix(out, data, 4, 4, &v.x, 1, 4);
	return Spam::Vector4(out);
}
Spam::Matrix4 Spam::Matrix4::operator*(float v)
{
	return Matrix4(
		data[0] * v,
		data[1] * v,
		data[2] * v,
		data[3] * v,
		data[4] * v,
		data[5] * v,
		data[6] * v,
		data[7] * v,
		data[8] * v,
		data[9] * v,
		data[10] * v,
		data[11] * v,
		data[12] * v,
		data[13] * v,
		data[14] * v,
		data[15] * v
		);
}
Spam::Matrix4 Spam::Matrix4::operator/(float v)
{
	return Matrix4(
		data[0] / v,
		data[1] / v,
		data[2] / v,
		data[3] / v,
		data[4] / v,
		data[5] / v,
		data[6] / v,
		data[7] / v,
		data[8] / v,
		data[9] / v,
		data[10] / v,
		data[11] / v,
		data[12] / v,
		data[13] / v,
		data[14] / v,
		data[15] / v
		);
}

void Spam::multiplyMatrix(float *out, float *m1, int w1, int h1, float *m2, int w2, int h2)
{
	for (int y = 0; y < h1; y++)
	{
		for (int x = 0; x < w2; x++)
		{
			out[y*w2 + x] = 0.0f;
			for (int i = 0; i < w1; i++)
			{
				out[y*w2 + x] += m1[y*w1 + i] * m2[i*w2 + x];
			}
		}
	}
}

void Spam::translate(Matrix4 &M, float x, float y, float z)
{
	M *= Matrix4(
		1, 0, 0, x,
			0, 1, 0, y,
			0, 0, 1, z,
			0, 0, 0, 1
	);

}
void Spam::rotate(Matrix4 &M, float a, float x, float y, float z)
{
	Matrix4 Rx = Matrix4(
		1, 0, 0, 0,
			0, cos(a*x), sin(a*x), 0,
			0, -sin(a*x), cos(a*x), 0,
			0, 0, 0, 1
	);
	Matrix4 Ry = Matrix4(
		cos(a*y), 0, -sin(a*y), 0,
			0, 1, 0, 0,
			sin(a*y), 0, cos(a*y), 0,
			0, 0, 0, 1
	);
	Matrix4 Rz = Matrix4(
		cos(a*z), -sin(a*z), 0, 0,
			sin(a*z), cos(a*z), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
	);
	M *= Rx*Ry*Rz;
}
void Spam::scale(Matrix4 &M, float x, float y, float z)
{
	M *= Matrix4(
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1
	);
}
Spam::Matrix4 Spam::ortho(float l, float r, float b, float t, float n, float f)
{
	return Matrix4(
		2 / (r - l), 0, 0, -(r + l) / (r - l),
		0, 2 / (t - b), 0, -(t + b) / (t - b),
		0, 0, -2 / (f - n), -(f + n) / (f - n),
		0, 0, 0, 1
	);
}
Spam::Matrix4 Spam::frustum(float l, float r, float b, float t, float n, float f)
{
	return Spam::Matrix4(
		(2 * n) / (r - l), 0, (r + l) / (r - l), 0,
		0, (2 * n) / (t - b), (t + b) / (t - b), 0,
		0, 0, -(f + n) / (f - n), -(2 * n*f) / (f - n),
		0, 0, -1, 0
	);
}
Spam::Matrix4 Spam::inverse(Spam::Matrix4 M)
{
	float det =
			M.data[0] * M.data[5] * M.data[10] * M.data[15] + M.data[0] * M.data[6] * M.data[11] * M.data[13] + M.data[0] * M.data[7] * M.data[9] * M.data[14] +
			M.data[1] * M.data[4] * M.data[11] * M.data[14] + M.data[1] * M.data[6] * M.data[8] * M.data[15] + M.data[1] * M.data[7] * M.data[10] * M.data[12] +
			M.data[2] * M.data[4] * M.data[9] * M.data[15] + M.data[2] * M.data[5] * M.data[11] * M.data[12] + M.data[2] * M.data[7] * M.data[8] * M.data[13] +
			M.data[3] * M.data[4] * M.data[10] * M.data[13] + M.data[3] * M.data[5] * M.data[8] * M.data[14] + M.data[3] * M.data[6] * M.data[9] * M.data[12] -
			M.data[0] * M.data[5] * M.data[11] * M.data[14] - M.data[0] * M.data[6] * M.data[9] * M.data[15] - M.data[0] * M.data[7] * M.data[10] * M.data[13] -
			M.data[1] * M.data[4] * M.data[10] * M.data[15] - M.data[1] * M.data[6] * M.data[11] * M.data[12] - M.data[1] * M.data[7] * M.data[8] * M.data[14] -
			M.data[2] * M.data[4] * M.data[11] * M.data[13] - M.data[2] * M.data[5] * M.data[8] * M.data[15] - M.data[2] * M.data[7] * M.data[9] * M.data[12] -
			M.data[3] * M.data[4] * M.data[9] * M.data[14] - M.data[3] * M.data[5] * M.data[10] * M.data[12] - M.data[3] * M.data[6] * M.data[8] * M.data[13];
	if (det == 0.0f) return Spam::Matrix4(1.0f);
	return Spam::Matrix4(
			(1 / det)*(M[5] * M[10] * M[15] + M[6] * M[11] * M[13] + M[7] * M[9] * M[14] - M[5] * M[11] * M[14] - M[6] * M[9] * M[15] - M[7] * M[10] * M[13]),
			(1 / det)*(M[1] * M[11] * M[14] + M[2] * M[9] * M[15] + M[3] * M[10] * M[13] - M[1] * M[10] * M[15] - M[2] * M[11] * M[13] - M[3] * M[9] * M[14]),
			(1 / det)*(M[1] * M[6] * M[15] + M[2] * M[7] * M[13] + M[3] * M[5] * M[14] - M[1] * M[7] * M[14] - M[2] * M[5] * M[15] - M[3] * M[6] * M[13]),
			(1 / det)*(M[1] * M[7] * M[10] + M[2] * M[5] * M[11] + M[3] * M[6] * M[9] - M[1] * M[6] * M[11] - M[2] * M[7] * M[9] - M[3] * M[5] * M[10]),

			(1 / det)*(M[4] * M[11] * M[14] + M[6] * M[8] * M[15] + M[7] * M[10] * M[12] - M[4] * M[10] * M[15] - M[6] * M[11] * M[12] - M[7] * M[8] * M[14]),
			(1 / det)*(M[0] * M[10] * M[15] + M[2] * M[11] * M[12] + M[3] * M[8] * M[14] - M[0] * M[11] * M[14] - M[2] * M[8] * M[15] - M[3] * M[10] * M[12]),
			(1 / det)*(M[0] * M[7] * M[14] + M[2] * M[4] * M[15] + M[3] * M[6] * M[12] - M[0] * M[6] * M[15] - M[2] * M[7] * M[12] - M[3] * M[4] * M[14]),
			(1 / det)*(M[0] * M[6] * M[11] + M[2] * M[7] * M[8] + M[3] * M[4] * M[10] - M[0] * M[7] * M[10] - M[2] * M[4] * M[11] - M[3] * M[6] * M[8]),

			(1 / det)*(M[4] * M[9] * M[15] + M[5] * M[11] * M[12] + M[7] * M[8] * M[13] - M[4] * M[11] * M[13] - M[5] * M[8] * M[15] - M[7] * M[9] * M[12]),
			(1 / det)*(M[0] * M[11] * M[13] + M[1] * M[8] * M[15] + M[3] * M[9] * M[12] - M[0] * M[9] * M[15] - M[1] * M[11] * M[12] - M[3] * M[8] * M[13]),
			(1 / det)*(M[0] * M[5] * M[15] + M[1] * M[7] * M[12] + M[3] * M[4] * M[13] - M[0] * M[7] * M[13] - M[1] * M[4] * M[15] - M[3] * M[5] * M[12]),
			(1 / det)*(M[0] * M[7] * M[9] + M[1] * M[4] * M[11] + M[3] * M[5] * M[8] - M[0] * M[5] * M[11] - M[1] * M[7] * M[8] - M[3] * M[4] * M[9]),

			(1 / det)*(M[4] * M[10] * M[13] + M[5] * M[8] * M[14] + M[6] * M[9] * M[12] - M[4] * M[9] * M[14] - M[5] * M[10] * M[12] - M[6] * M[8] * M[13]),
			(1 / det)*(M[0] * M[9] * M[14] + M[1] * M[10] * M[12] + M[2] * M[8] * M[13] - M[0] * M[10] * M[13] - M[1] * M[8] * M[14] - M[2] * M[9] * M[12]),
			(1 / det)*(M[0] * M[6] * M[13] + M[1] * M[4] * M[14] + M[2] * M[5] * M[12] - M[0] * M[5] * M[14] - M[1] * M[6] * M[12] - M[2] * M[4] * M[13]),
			(1 / det)*(M[0] * M[5] * M[10] + M[1] * M[6] * M[8] + M[2] * M[4] * M[9] - M[0] * M[6] * M[9] - M[1] * M[4] * M[10] - M[2] * M[5] * M[8])
	);
}