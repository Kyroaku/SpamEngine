#include "Camera.h"

#include <math.h>

//Camera
Spam::Camera::Camera()
{
	position = Spam::Vector3(0.0f, 0.0f, 0.0f);
	target = Spam::Vector3(0.0f, 0.0f, -1.0f);
	rotation = Spam::Vector3(0.0f, 0.0f, 0.0f);
}

//CameraFps
Spam::CameraFps::CameraFps()
{
	position = Spam::Vector3(0.0f, 0.0f, 0.0f);
	target = Spam::Vector3(0.0f, 0.0f, -1.0f);
	rotation = Spam::Vector3(0.0f, 0.0f, 0.0f);
}
Spam::Matrix4 Spam::CameraFps::getMatrix()
{
	Spam::Matrix4 mat(1.0f);
	Spam::rotate(mat, 1.0f, -rotation.x, -rotation.y, -rotation.z);
	Spam::translate(mat, -position.x, -position.y, -position.z);
	return mat;
}
void Spam::CameraFps::setPosition(float x, float y, float z)
{
	position = Spam::Vector3(x, y, z);
}
void Spam::CameraFps::setRotation(float x, float y, float z)
{
	rotation = Spam::Vector3(x, y, z);
}
void Spam::CameraFps::move(float x, float y, float z)
{
	position += Spam::Vector3(x, y, z);
}
void Spam::CameraFps::moveForward(float s)
{
	position += Spam::Vector3((float)-sin(rotation.y)*-s, 0.0f, (float)cos(rotation.y)*-s);
}
void Spam::CameraFps::moveBackward(float s)
{
	position += Spam::Vector3((float)-sin(rotation.y)*s, 0.0f, (float)cos(rotation.y)*s);
}
void Spam::CameraFps::moveLeft(float s)
{
	position += Spam::Vector3((float)-sin(rotation.y + 1.57f)*s, 0.0f, (float)cos(rotation.y + 1.57f)*s);
}
void Spam::CameraFps::moveRight(float s)
{
	position += Spam::Vector3((float)-sin(rotation.y - 1.57f)*s, 0.0f, (float)cos(rotation.y - 1.57f)*s);
}
void Spam::CameraFps::moveUp(float s)
{
	position.y += s;
}
void Spam::CameraFps::moveDown(float s)
{
	position.y -= s;
}
void Spam::CameraFps::rotate(float x, float y, float z)
{
	rotation += Spam::Vector3(x, y, z);
}