#include "Shader.h"

Spam::Shader::Shader(const char *vertexFilename, const char *fragmentFilename)
{
	std::string vertexShaderStr, fragmentShaderStr;
	char *vertexShaderSrc, *fragmentShaderSrc;
	GLuint vertexShader, fragmentShader;
	char infoLog[1024];

	vertexShaderStr = loadFromFile(vertexFilename);
	fragmentShaderStr = loadFromFile(fragmentFilename);
	if (vertexShaderStr.size() > 0 && fragmentShaderStr.size() > 0)
	{
		vertexShaderSrc = new char[vertexShaderStr.size()];
		fragmentShaderSrc = new char[fragmentShaderStr.size()];
		strcpy_s(vertexShaderSrc, vertexShaderStr.size() + 1, vertexShaderStr.c_str());
		strcpy_s(fragmentShaderSrc, fragmentShaderStr.size() + 1, fragmentShaderStr.c_str());

		vertexShader = glCreateShader(GL_VERTEX_SHADER);
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

		glShaderSource(vertexShader, 1, (const char**)&vertexShaderSrc, 0);
		glShaderSource(fragmentShader, 1, (const char**)&fragmentShaderSrc, 0);

		glCompileShader(vertexShader);
		glCompileShader(fragmentShader);

		program = glCreateProgram();
		glAttachShader(program, vertexShader);
		glAttachShader(program, fragmentShader);
		/*glBindAttribLocation(program, 0, "a_Position");
		glBindAttribLocation(program, 1, "a_Color");
		glBindAttribLocation(program, 2, "a_TextureCoord");
		glBindAttribLocation(program, 3, "a_Normal");
		glBindAttribLocation(program, 4, "a_Tangent");
		glBindAttribLocation(program, 5, "a_Binormal");*/
		glLinkProgram(program);

		glGetShaderInfoLog(vertexShader, 1024, 0, infoLog);
		if (strlen(infoLog) > 0)
			printf("VertexShader Log:\n%s\n", infoLog);
		glGetShaderInfoLog(fragmentShader, 1024, 0, infoLog);
		if (strlen(infoLog) > 0)
			printf("FragmentShader Log:\n%s\n", infoLog);
		glGetProgramInfoLog(program, 1024, 0, infoLog);
		if (strlen(infoLog) > 0)
			printf("ShaderProgram Log:\n%s\n", infoLog);

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}
}
Spam::Shader::~Shader()
{
	glDeleteProgram(program);
}
std::string Spam::Shader::loadFromFile(std::string filename)
{
	FILE *file;
	std::string fileSrc="";
	char line[512];
	fopen_s(&file, &filename.at(0), "rt");
	if (file != 0) {
		while (!feof(file))
		{
			fgets(line, 512, file);
			fileSrc += line;
		}
		fclose(file);
	}
	else {
		printf("Can't open file: %s\n", filename.c_str());
		return "";
	}
	return fileSrc;
}
void Spam::Shader::use()
{
	glUseProgram(program);
}
int Spam::Shader::getProgram()
{
	return program;
}

void Spam::Shader::uniform1i(const char *loc, int v)
{
	glUniform1i(glGetUniformLocation(program, loc), v);
}
void Spam::Shader::uniform2i(const char *loc, int v1, int v2)
{
	glUniform2i(glGetUniformLocation(program, loc), v1, v2);
}
void Spam::Shader::uniform3i(const char *loc, int v1, int v2, int v3)
{
	glUniform3i(glGetUniformLocation(program, loc), v1, v2, v3);
}
void Spam::Shader::uniform4i(const char *loc, int v1, int v2, int v3, int v4)
{
	glUniform4i(glGetUniformLocation(program, loc), v1, v2, v3, v4);
}
void Spam::Shader::uniform2iv(const char *loc, int *v, int count)
{
	glUniform2iv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform3iv(const char *loc, int *v, int count)
{
	glUniform3iv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform4iv(const char *loc, int *v, int count)
{
	glUniform4iv(glGetUniformLocation(program, loc), count, v);
}

void Spam::Shader::uniform1ui(const char *loc, unsigned int v)
{
	glUniform1ui(glGetUniformLocation(program, loc), v);
}
void Spam::Shader::uniform2ui(const char *loc, unsigned int v1, unsigned int v2)
{
	glUniform2ui(glGetUniformLocation(program, loc), v1, v2);
}
void Spam::Shader::uniform3ui(const char *loc, unsigned int v1, unsigned int v2, unsigned int v3)
{
	glUniform3ui(glGetUniformLocation(program, loc), v1, v2, v3);
}
void Spam::Shader::uniform4ui(const char *loc, unsigned int v1, unsigned int v2, unsigned int v3, unsigned int v4)
{
	glUniform4ui(glGetUniformLocation(program, loc), v1, v2, v3, v4);
}
void Spam::Shader::uniform2uiv(const char *loc, unsigned int *v, int count)
{
	glUniform2uiv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform3uiv(const char *loc, unsigned int *v, int count)
{
	glUniform3uiv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform4uiv(const char *loc, unsigned int *v, int count)
{
	glUniform4uiv(glGetUniformLocation(program, loc), count, v);
}

void Spam::Shader::uniform1f(const char *loc, float v)
{
	glUniform1f(glGetUniformLocation(program, loc), v);
}
void Spam::Shader::uniform2f(const char *loc, float v1, float v2)
{
	glUniform2f(glGetUniformLocation(program, loc), v1, v2);
}
void Spam::Shader::uniform3f(const char *loc, float v1, float v2, float v3)
{
	glUniform3f(glGetUniformLocation(program, loc), v1, v2, v3);
}
void Spam::Shader::uniform4f(const char *loc, float v1, float v2, float v3, float v4)
{
	glUniform4f(glGetUniformLocation(program, loc), v1, v2, v3, v4);
}
void Spam::Shader::uniform2fv(const char *loc, float *v, int count)
{
	glUniform2fv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform3fv(const char *loc, float *v, int count)
{
	glUniform3fv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform4fv(const char *loc, float *v, int count)
{
	glUniform4fv(glGetUniformLocation(program, loc), count, v);
}

void Spam::Shader::uniform1d(const char *loc, double v)
{
	glUniform1d(glGetUniformLocation(program, loc), v);
}
void Spam::Shader::uniform2d(const char *loc, double v1, double v2)
{
	glUniform2d(glGetUniformLocation(program, loc), v1, v2);
}
void Spam::Shader::uniform3d(const char *loc, double v1, double v2, double v3)
{
	glUniform3d(glGetUniformLocation(program, loc), v1, v2, v3);
}
void Spam::Shader::uniform4d(const char *loc, double v1, double v2, double v3, double v4)
{
	glUniform4d(glGetUniformLocation(program, loc), v1, v2, v3, v4);
}
void Spam::Shader::uniform2dv(const char *loc, double *v, int count)
{
	glUniform2dv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform3dv(const char *loc, double *v, int count)
{
	glUniform3dv(glGetUniformLocation(program, loc), count, v);
}
void Spam::Shader::uniform4dv(const char *loc, double *v, int count)
{
	glUniform4dv(glGetUniformLocation(program, loc), count, v);
}

void Spam::Shader::uniformMatrix2x3fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix2x3fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix2x4fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix2x4fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix3x2fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix3x2fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix3x4fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix3x4fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix4x2fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix4x2fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix4x3fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix4x3fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix2fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix2fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix3fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix3fv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix4fv(const char *loc, bool transpose, float *v, int count)
{
	glUniformMatrix4fv(glGetUniformLocation(program, loc), count, transpose, v);
}

void Spam::Shader::uniformMatrix2x3dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix2x3dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix2x4dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix2x4dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix3x2dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix3x2dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix3x4dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix3x4dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix4x2dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix4x2dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix4x3dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix4x3dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix2dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix2dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix3dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix3dv(glGetUniformLocation(program, loc), count, transpose, v);
}
void Spam::Shader::uniformMatrix4dv(const char *loc, bool transpose, double *v, int count)
{
	glUniformMatrix4dv(glGetUniformLocation(program, loc), count, transpose, v);
}