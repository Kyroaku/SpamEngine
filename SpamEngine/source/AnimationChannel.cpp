#include "AnimationChannel.h"

Spam::AnimationChannel::AnimationChannel() :
node(0),
numKeys(0),
keys(0)
{

}
Spam::AnimationChannel::~AnimationChannel()
{
	if (node) delete node;

	if (keys && numKeys > 0)
	for (int i = numKeys - 1; i >= 0; i--)
	if (keys[i])
		delete keys[i];
}