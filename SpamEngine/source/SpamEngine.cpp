#include "SpamEngine.h"

/*
TODO:
= update Camera class
	- change position, target and rotation types to 'Vector3'
= update Matrix and Vector classes
	- don't use 'multiplyMatrix' in Matrix classes. Use 'data[i]'
	- add more types
	- add math functions
	*/

Spam::Engine::Engine(const char *title)
{
	window = new Spam::Window(title, 800, 600, 16, false);
	init();
}
Spam::Engine::Engine(const char *title, int width, int height)
{
	window = new Spam::Window(title, width, height, 16, false);
	init();
}
Spam::Engine::Engine(const char *title, int width, int height, short bitsPerPixel, bool fullscreen)
{
	window = new Spam::Window(title, width, height, bitsPerPixel, fullscreen);
	init();
}
Spam::Engine::~Engine()
{
	if (window) delete window;
	if (renderer) delete renderer;
	if (mouse) delete mouse;
	if (keyboard) delete keyboard;
}
void Spam::Engine::init()
{
	renderer = 0;
	keyboard = 0;
	mouse = 0;

	if (glewInit() != GLEW_OK)
		printf("glewInit() error.\n");
	printf("OpenGL version: %s\n", glGetString(GL_VERSION));
	printf("OpenGL shader version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
}
void Spam::Engine::realTimeLoop()
{
	SDL_Event event; //window event
	fps.lastFrameTime = SDL_GetTicks();
	while (!bQuit)
	{
		//getting time to calculate delay and FPS
		float deltaTime = (float)(SDL_GetTicks() - fps.lastFrameTime) / 1000.0f;
		fps.lastFrameTime = SDL_GetTicks();
		fps.update(deltaTime);
		//Real-Time loop
		//Getting window events
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_WINDOWEVENT:
				if (event.window.event == SDL_WINDOWEVENT_RESIZED && renderer != NULL)
					renderer->onResize(event.window.data1, event.window.data2);
				break;

			case SDL_KEYDOWN:
				if (keyboard != 0) {
					keyboard->keys[event.key.keysym.sym] = true;
					keyboard->onKeyDown(event.key.keysym.sym);
				}
				break;

			case SDL_KEYUP:
				if (keyboard != 0) {
					keyboard->keys[event.key.keysym.sym] = false;
					keyboard->onKeyUp(event.key.keysym.sym);
				}
				break;

			case SDL_MOUSEBUTTONDOWN:
				if (mouse != 0) {
					mouse->buttons[event.button.button] = true;
					mouse->onButtonDown(event.button.button);
				}
				break;

			case SDL_MOUSEBUTTONUP:
				if (mouse != 0) {
					mouse->buttons[event.button.button] = false;
					mouse->onButtonUp(event.button.button);
				}
				break;

			case SDL_MOUSEMOTION:
				if (mouse != 0) {
					mouse->x = event.motion.x;
					mouse->y = event.motion.y;
					mouse->onMove(event.motion.x, event.motion.y, event.motion.xrel, event.motion.yrel);
				}
				break;

			case SDL_MOUSEWHEEL:
				if (mouse != 0) {
					mouse->onWheel(event.wheel.x, event.wheel.y);
				}
				break;

			case SDL_QUIT:
				bQuit = true;
				break;
			}
		}
		if (renderer != NULL) {
			renderer->onUpdate(deltaTime);
			renderer->onRender();
		}
		//Swap buffers (double-buffering)
		SDL_GL_SwapWindow(window->getHandle());

		Sleep(0);
	}
}
void Spam::Engine::setRenderer(Spam::Renderer *renderer) {
	this->renderer = renderer;
	int windowWidth, windowHeight;
	SDL_GetWindowSize(window->getHandle(), &windowWidth, &windowHeight);
	this->renderer->onResize(windowWidth, windowHeight);
}
void Spam::Engine::setKeyboard(Spam::Keyboard *keyboard)
{
	this->keyboard = keyboard;
}
void Spam::Engine::setMouse(Spam::Mouse *mouse)
{
	this->mouse = mouse;
}
Spam::Renderer *Spam::Engine::getRenderer() {
	return this->renderer;
}
Spam::Keyboard *Spam::Engine::getKeyboard()
{
	return this->keyboard;
}
Spam::Mouse *Spam::Engine::getMouse()
{
	return this->mouse;
}
Spam::Window *Spam::Engine::getWindow() {
	return window;
}
void Spam::Engine::enableRelativeMouseMode()
{
	SDL_SetRelativeMouseMode(SDL_TRUE);
}
void Spam::Engine::disableRelativeMouseMode()
{
	SDL_SetRelativeMouseMode(SDL_FALSE);
}
bool Spam::Engine::isRelativeMouseMode()
{
	return SDL_GetRelativeMouseMode() == SDL_TRUE ? true : false;
}
int Spam::Engine::getFps() {
	return fps.fps;
}

void Spam::Engine::quit() {
	bQuit = true;
}

void Spam::Engine::bindTexture(Spam::Texture *texture, int i)
{
	glActiveTexture(GL_TEXTURE0 + i);
	glBindTexture(GL_TEXTURE_2D, texture->getTexture());
}

void Spam::Engine::render(Spam::Scene *scene, Spam::Mesh *mesh, Spam::Material *material)
{
	if (mesh->vertexPositions != 0) {
		glEnableVertexAttribArray(SPAM_POSITION_ARRAY_INDEX);
		glVertexAttribPointer(SPAM_POSITION_ARRAY_INDEX, 3, GL_FLOAT, false, 0, &mesh->vertexPositions[0].x);
	}
	if (mesh->vertexTextureCoords != 0) {
		glEnableVertexAttribArray(SPAM_TEXTURECOORD_ARRAY_INDEX);
		glVertexAttribPointer(SPAM_TEXTURECOORD_ARRAY_INDEX, 3, GL_FLOAT, false, 0, &mesh->vertexTextureCoords[0].x);
	}
	if (mesh->vertexColors != 0) {
		glEnableVertexAttribArray(SPAM_COLOR_ARRAY_INDEX);
		glVertexAttribPointer(SPAM_COLOR_ARRAY_INDEX, 4, GL_FLOAT, false, 0, &mesh->vertexColors[0].x);
	}
	if (mesh->vertexNormals != 0) {
		glEnableVertexAttribArray(SPAM_NORMAL_ARRAY_INDEX);
		glVertexAttribPointer(SPAM_NORMAL_ARRAY_INDEX, 3, GL_FLOAT, false, 0, &mesh->vertexNormals[0].x);
	}
	if (mesh->vertexTangents != 0) {
		glEnableVertexAttribArray(SPAM_TANGENT_ARRAY_INDEX);
		glVertexAttribPointer(SPAM_TANGENT_ARRAY_INDEX, 3, GL_FLOAT, false, 0, &mesh->vertexTangents[0].x);
	}
	if (mesh->vertexBitangents != 0) {
		glEnableVertexAttribArray(SPAM_BITANGENT_ARRAY_INDEX);
		glVertexAttribPointer(SPAM_BITANGENT_ARRAY_INDEX, 3, GL_FLOAT, false, 0, &mesh->vertexBitangents[0].x);
	}
	if (mesh->boneIds != 0) {
		glEnableVertexAttribArray(SPAM_BONE_ID_ARRAY_INDEX);
		glVertexAttribIPointer(SPAM_BONE_ID_ARRAY_INDEX, 4, GL_UNSIGNED_INT, 0, &mesh->boneIds[0]);
	}
	if (mesh->boneWeights != 0) {
		glEnableVertexAttribArray(SPAM_BONE_WEIGHT_ARRAY_INDEX);
		glVertexAttribPointer(SPAM_BONE_WEIGHT_ARRAY_INDEX, 4, GL_FLOAT, false, 0, &mesh->boneWeights[0]);
	}
	if (mesh->vertexIndices) {
		glDrawElements(GL_TRIANGLES, mesh->numIndices, GL_UNSIGNED_INT, &mesh->vertexIndices[0]);
	}
	else {
		glDrawArrays(GL_TRIANGLES, 0, mesh->numVertices);
	}
}
void Spam::Engine::render(Spam::Scene *scene, Spam::Node *node, Spam::Shader *shader, Spam::Matrix4 transformation)
{
	/* render meshes */
	loop(mesh_id, node->numMeshes)
	{
		loop(bone_id, scene->meshes[node->meshes[mesh_id]]->numBones)
		{
			scene->meshes[node->meshes[mesh_id]]->boneMatrices[bone_id] =
				scene->meshes[node->meshes[mesh_id]]->bones[bone_id]->node->getAbsoluteMatrix() * scene->meshes[node->meshes[mesh_id]]->bones[bone_id]->bindposeMatrix;
		}
		shader->uniformMatrix4fv("se_ModelMatrix", true, (transformation*node->transformation).get());
		shader->uniformMatrix4fv("se_BoneMatrices", true, &scene->meshes[node->meshes[mesh_id]]->boneMatrices[0].data[0], scene->meshes[node->meshes[mesh_id]]->numBones);
		render(scene, scene->meshes[node->meshes[mesh_id]], scene->materials[scene->meshes[node->meshes[mesh_id]]->material]);
	}

	/* render childrens */
	loop(child_id, node->numChildrens)
	{
		render(scene, node->childrens[child_id], shader, transformation * node->transformation);
	}
}
void Spam::Engine::render(Spam::Scene *scene, Spam::Shader *shader)
{
	render(scene, scene->rootNode, shader);
}