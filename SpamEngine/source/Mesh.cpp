#include "Mesh.h"

Spam::Mesh::Mesh() :
vertexPositions(0),
vertexColors(0),
vertexTextureCoords(0),
vertexNormals(0),
vertexTangents(0),
vertexBitangents(0),
numVertices(0),
vertexIndices(0),
numIndices(0),
boneIds(0),
boneWeights(0),
bones(0),
boneMatrices(0),
material(0)
{

}
Spam::Mesh::~Mesh()
{
	if (vertexPositions) delete vertexPositions;
	if (vertexColors) delete vertexColors;
	if (vertexTextureCoords) delete vertexTextureCoords;
	if (vertexNormals) delete vertexNormals;
	if (vertexTangents) delete vertexTangents;
	if (vertexBitangents) delete vertexBitangents;
	if (vertexIndices) delete vertexIndices;
	if (boneIds) delete boneIds;
	if (boneWeights) delete boneWeights;
	
	if (bones && numBones > 0)
	for (int i = numBones - 1; i >= 0; i--)
	if (bones[i])
		delete bones[i];

	if (boneMatrices) delete boneMatrices;
}