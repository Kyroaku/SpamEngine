#include "Material.h"

Spam::Material::Material() :
ambient(0.0f, 0.0f, 0.0f),
diffuse(0.0f, 0.0f, 0.0f),
specular(0.0f, 0.0f, 0.0f),
emissive(0.0f, 0.0f, 0.0f),
shininess(0.0f),
texture(0)
{

}
Spam::Material::~Material()
{
	if (texture) delete texture;
}