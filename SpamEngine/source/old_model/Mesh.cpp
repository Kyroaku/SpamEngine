#include "Mesh.h"

Spam::Mesh::Mesh()
{
	translation = Spam::Vector4(0.0f, 0.0f, 0.0f, 1.0f);
	rotation = Spam::Vector4(0.0f, 0.0f, 0.0f, 0.0f);
	scale = Spam::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	geometry = 0;
	texture = 0;
	childs = 0;
	numChilds = 0;
}
Spam::Mesh::Mesh(Spam::Geometry *geometry, Spam::Texture *texture)
{
	translation = Spam::Vector4(0.0f, 0.0f, 0.0f, 1.0f);
	rotation = Spam::Vector4(0.0f, 0.0f, 0.0f, 0.0f);
	scale = Spam::Vector4(1.0f, 1.0f, 1.0f, 1.0f);
	setGeometry(geometry);
	setTexture(texture);
	childs = 0;
	numChilds = 0;
}
Spam::Mesh::~Mesh()
{
	if (geometry) delete geometry;
	if (texture) delete texture;
}
void Spam::Mesh::setTranslation(float x, float y, float z)
{
	translation.x = x;
	translation.y = y;
	translation.z = z;
}
void Spam::Mesh::setRotation(float x, float y, float z)
{
	rotation.x = x;
	rotation.y = y;
	rotation.z = z;
}
void Spam::Mesh::setScale(float x, float y, float z)
{
	scale.x = x;
	scale.y = y;
	scale.z = z;
}
void Spam::Mesh::setTranslation(Spam::Vector4 v)
{
	translation = v;
}
void Spam::Mesh::setRotation(Spam::Vector4 v)
{
	rotation = v;
}
void Spam::Mesh::setScale(Spam::Vector4 v)
{
	scale = v;
}
Spam::Vector4 Spam::Mesh::getTranslation()
{
	return translation;
}
Spam::Vector4 Spam::Mesh::getRotation()
{
	return rotation;
}
Spam::Vector4 Spam::Mesh::getScale()
{
	return scale;
}
void Spam::Mesh::setGeometry(Spam::Geometry *geometry)
{
	this->geometry = geometry;
}
void Spam::Mesh::setTexture(Spam::Texture *texture)
{
	this->texture = texture;
}
Spam::Geometry *Spam::Mesh::getGeometry()
{
	return geometry;
}
Spam::Texture *Spam::Mesh::getTexture()
{
	return texture;
}
bool Spam::Mesh::hasGeometry()
{
	return geometry != 0;
}
bool Spam::Mesh::hasTexture()
{
	return texture != 0;
}
void Spam::Mesh::initChildrens(int n)
{
	numChilds = n;
	childs = new Spam::Mesh*[n];
	for (int i = 0; i < n; i++)
		childs[i] = new Spam::Mesh();
}
void Spam::Mesh::setChildren(int i, Spam::Mesh *child)
{
	childs[i] = child;
}
Spam::Mesh *Spam::Mesh::getChildren(int i)
{
	return childs[i];
}
int Spam::Mesh::getChildrenCount()
{
	return numChilds;
}