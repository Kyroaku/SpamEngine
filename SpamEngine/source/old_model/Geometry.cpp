#include "Geometry.h"

Spam::Geometry::Geometry()
{
	vertexPositions = 0;
	vertexColors = 0;
	vertexTextureCoords = 0;
	vertexNormals = 0;
	vertexTangents = 0;
	vertexBitangents = 0;
	vertexIndices = 0;
	polygonMode = GL_TRIANGLES;
	numIndices = 0;
	numVertices = 0;
}
Spam::Geometry::~Geometry()
{
	if (vertexPositions) delete vertexPositions;
	if (vertexColors) delete vertexColors;
	if (vertexTextureCoords) delete vertexTextureCoords;
	if (vertexNormals) delete vertexNormals;
	if (vertexTangents) delete vertexTangents;
	if (vertexBitangents) delete vertexBitangents;
	if (vertexIndices) delete vertexIndices;
}
void Spam::Geometry::initVertexPositions(int n)
{
	if (vertexPositions) delete vertexPositions;
	vertexPositions = new float[n * 4];
	numVertices = n;
}
void Spam::Geometry::initVertexColors(int n)
{
	if (vertexColors) delete vertexColors;
	vertexColors = new float[n * 4];
}
void Spam::Geometry::initVertexTextureCoords(int n)
{
	if (vertexTextureCoords) delete vertexTextureCoords;
	vertexTextureCoords = new float[n * 3];
}
void Spam::Geometry::initVertexNormals(int n)
{
	if (vertexNormals) delete vertexNormals;
	vertexNormals = new float[n * 3];
}
void Spam::Geometry::initVertexIndices(int n)
{
	if (vertexIndices) delete vertexIndices;
	vertexIndices = new int[n];
	numIndices = n;
}
void Spam::Geometry::setVertexPositions(Spam::Vector4 *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexPositions[i * 4 + 0] = v[i].x;
		vertexPositions[i * 4 + 1] = v[i].y;
		vertexPositions[i * 4 + 2] = v[i].z;
		vertexPositions[i * 4 + 3] = v[i].w;
	}
}
void Spam::Geometry::setVertexColors(Spam::Vector4 *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexColors[i * 4 + 0] = v[i].x;
		vertexColors[i * 4 + 1] = v[i].y;
		vertexColors[i * 4 + 2] = v[i].z;
		vertexColors[i * 4 + 3] = v[i].w;
	}
}
void Spam::Geometry::setVertexTextureCoords(Spam::Vector4 *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexTextureCoords[i * 3 + 0] = v[i].x;
		vertexTextureCoords[i * 3 + 1] = v[i].y;
		vertexTextureCoords[i * 3 + 2] = v[i].z;
	}
}
void Spam::Geometry::setVertexNormals(Spam::Vector4 *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexNormals[i * 3 + 0] = v[i].x;
		vertexNormals[i * 3 + 1] = v[i].y;
		vertexNormals[i * 3 + 2] = v[i].z;
	}
}
void Spam::Geometry::setVertexPositions(float *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexPositions[i * 4 + 0] = v[i * 4 + 0];
		vertexPositions[i * 4 + 1] = v[i * 4 + 1];
		vertexPositions[i * 4 + 2] = v[i * 4 + 2];
		vertexPositions[i * 4 + 3] = v[i * 4 + 3];
	}
}
void Spam::Geometry::setVertexColors(float *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexColors[i * 4 + 0] = v[i * 4 + 0];
		vertexColors[i * 4 + 1] = v[i * 4 + 1];
		vertexColors[i * 4 + 2] = v[i * 4 + 2];
		vertexColors[i * 4 + 3] = v[i * 4 + 3];
	}
}
void Spam::Geometry::setVertexTextureCoords(float *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexTextureCoords[i * 3 + 0] = v[i * 3 + 0];
		vertexTextureCoords[i * 3 + 1] = v[i * 3 + 1];
		vertexTextureCoords[i * 3 + 2] = v[i * 3 + 2];
	}
}
void Spam::Geometry::setVertexNormals(float *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexNormals[i * 3 + 0] = v[i * 3 + 0];
		vertexNormals[i * 3 + 1] = v[i * 3 + 1];
		vertexNormals[i * 3 + 2] = v[i * 3 + 2];
	}
}
void Spam::Geometry::setVertexIndices(int *v, int n)
{
	for (int i = 0; i < n; i++) {
		vertexIndices[i] = v[i];
	}
}
void Spam::Geometry::setPolygonMode(int mode)
{
	polygonMode = mode;
}
float *Spam::Geometry::getVertexPositionsData()
{
	return vertexPositions;
}
float *Spam::Geometry::getVertexColorsData()
{
	return vertexColors;
}
float *Spam::Geometry::getVertexTextureCoordsData()
{
	return vertexTextureCoords;
}
float *Spam::Geometry::getVertexNormalsData()
{
	return vertexNormals;
}
int *Spam::Geometry::getVertexIndicesData()
{
	return vertexIndices;
}
int Spam::Geometry::getPolygonMode()
{
	return polygonMode;
}
bool Spam::Geometry::hasVertexPositions()
{
	return vertexPositions ? true : false;
}
bool Spam::Geometry::hasVertexColors()
{
	return vertexColors ? true : false;
}
bool Spam::Geometry::hasVertexTextureCoords()
{
	return vertexTextureCoords ? true : false;
}
bool Spam::Geometry::hasVertexNormals()
{
	return vertexNormals ? true : false;
}
bool Spam::Geometry::hasVertexIndices()
{
	return vertexIndices ? true : false;
}
int Spam::Geometry::getNumVertices()
{
	return numVertices;
}
int Spam::Geometry::getNumIndices()
{
	return numIndices;
}