#include "Model.h"

Spam::Model::Model()
{
	meshes = 0;
	numMeshes = 0;
}
Spam::Model::~Model()
{
	if (meshes) delete[] meshes;
}
void Spam::Model::initMeshes(int n)
{
	numMeshes = n;
	meshes = new Spam::Mesh*[n];
	for (int i = 0; i < n; i++)
		meshes[i] = new Spam::Mesh();
}
void Spam::Model::setMesh(int i, Spam::Mesh *mesh)
{
	meshes[i] = mesh;
}
Spam::Mesh *Spam::Model::getMesh(int i)
{
	return meshes[i];
}
int Spam::Model::getMeshCount()
{
	return numMeshes;
}