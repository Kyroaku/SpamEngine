#include "Window.h"

Spam::Window::Window(const char *title, int width, int height, short bitsPerPixel, bool fullscreen)
{
	if (init())
		create(title, width, height, bitsPerPixel, fullscreen);
}
Spam::Window::~Window()
{
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();
}
bool Spam::Window::init()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		printf("SDL_Init() error: %s\n", SDL_GetError());
		return false;
	}
	return true;
}
void Spam::Window::create(const char *title, int width, int height, short bitsPerPixel, bool fullscreen)
{
	if (fullscreen)
		window = SDL_CreateWindow(title, 50, 50, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_FULLSCREEN);
	else
		window = SDL_CreateWindow(title, 50, 50, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	if (window == NULL)
		printf("SDL_CreateWindow() error: %s\n", SDL_GetError());
	context = SDL_GL_CreateContext(window);
}
SDL_Window *Spam::Window::getHandle()
{
	return window;
}
SDL_GLContext Spam::Window::getGLContext()
{
	return context;
}
const char *Spam::Window::getTitle()
{
	return SDL_GetWindowTitle(window);
}
void Spam::Window::setTitle(const char *title)
{
	SDL_SetWindowTitle(window, title);
}
Spam::Vector2 Spam::Window::getPosition()
{
	int x, y;
	SDL_GetWindowPosition(window, &x, &y);
	return Spam::Vector2((float)x, (float)y);
}
void Spam::Window::setPosition(int x, int y)
{
	SDL_SetWindowPosition(window, x, y);
}
Spam::Vector2 Spam::Window::getSize()
{
	int w, h;
	SDL_GetWindowSize(window, &w, &h);
	return Spam::Vector2((float)w, (float)h);
}
void Spam::Window::setSize(int w, int h)
{
	SDL_SetWindowSize(window, w, h);
}
void Spam::Window::setMousePosition(int x, int y)
{
	SDL_WarpMouseInWindow(window, x, y);
}